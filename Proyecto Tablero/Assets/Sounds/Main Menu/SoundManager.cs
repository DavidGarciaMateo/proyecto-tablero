using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SoundManager : MonoBehaviour
{
   [SerializeField]
   private AudioMixerGroup fxGroup, musicGroup;

   [SerializeField] private Slider sliderFX, sliderMusic;

   private float volFx, volMusic;


    //private AudioMixer audioMixer;
    
    private bool muteFX = false, muteMusic = false;
    

    public void MuteFX(){
        if(muteFX == true){
            fxGroup.audioMixer.SetFloat("fxV",1f);
            muteFX = false;
        }else{
            fxGroup.audioMixer.SetFloat("fxV",-100f);
            muteFX = true;

        }
    }

    public void MuteMusic(){
         if(muteMusic == true){
            
            musicGroup.audioMixer.SetFloat("musicV",1f);
            muteMusic = false;
        }else{
            
            musicGroup.audioMixer.SetFloat("musicV",-100f);
            muteMusic = true;

        }
    }


    void Update(){
        
        //Estos IFs son por si el slider se pone a 0 que se mutee el sonido en cuestion
        if(sliderMusic.value == 0){

            musicGroup.audioMixer.SetFloat("musicV",-100f);
            
        }else if(sliderFX.value == 0){

            fxGroup.audioMixer.SetFloat("fxV",-100f);

        }
        

        
    }

    void Awake(){

        sliderMusic.value = PlayerPrefs.GetFloat("musicV");

        sliderFX.value = PlayerPrefs.GetFloat("fxV");
        
        sliderMusic.onValueChanged.AddListener(HandleSliderMusicValueChanged);

        sliderFX.onValueChanged.AddListener(HandleSliderFXValueChanged);
    }

    private void OnDisable(){
        //Pa guardar los valores de la musica y poder llamarlos en otro lado
        PlayerPrefs.SetFloat("musicV", sliderMusic.value);
        PlayerPrefs.SetFloat("fxV", sliderFX.value);
        
    }


    //Metodo para bajar el volumen de la musica
    private void HandleSliderMusicValueChanged(float value){

        musicGroup.audioMixer.SetFloat("musicV",Mathf.Log10(value)* 30f);
    }


    //Metodo para bajar el volumen de los efectos sonoros
    private void HandleSliderFXValueChanged(float value){
        fxGroup.audioMixer.SetFloat("fxV",Mathf.Log10(value)* 30f);
    }



}
