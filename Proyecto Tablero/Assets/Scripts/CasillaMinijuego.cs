using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CasillaMinijuego : MonoBehaviour
{

[SerializeField] private GameObject Pl1;
private bool done= false;

private void Start() {

  if(!PlayerPrefs.HasKey("Active"+gameObject.name))
  {
    PlayerPrefs.SetInt("Active"+gameObject.name,1);
  }

}
void OnTriggerEnter(Collider other)
{
    done=false;
}
  private void OnTriggerStay(Collider other) {
      
       if(PlayerPrefs.GetInt("Active"+gameObject.name)==1 && done==false && GameManager.Instance.movePlayer1.isOnRoute==false && GameManager.Instance.movePlayer2.isOnRoute == false)
       {
           PlayerPrefs.SetInt("Active"+gameObject.name,0);
           done= true;
           StartCoroutine(WaitMiniGame());
       }

  }
    IEnumerator WaitMiniGame()
  {
      yield return new WaitForSeconds(2);
      Pl1.GetComponent<CasillaDetection>().ChooseMinigame();
  
  }
}
