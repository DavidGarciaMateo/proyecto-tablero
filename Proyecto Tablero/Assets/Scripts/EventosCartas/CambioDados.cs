using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CambioDados : MonoBehaviour
{
    [SerializeField] private GameObject p1dado6;
    [SerializeField] private GameObject p1dado4;
    [SerializeField] private GameObject p2dado6;
    [SerializeField] private GameObject p2dado4;
    //gestiona las variables del gamemanager y los playerpref para realizar el cambio de dados

private void Start() {
    
}


public void ChangeDices()
{
    //la boolena eventDice será true al selecionar la carta
    if(GameManager.Instance.evenetDices == true)
    {
        GameManager.Instance.evenetDices = false;
        bool alreadyChange = false;

        if(alreadyChange==false && PlayerPrefs.GetString("1D6") == "P1")
        {
        alreadyChange = true;
        GameManager.Instance.movePlayer1.diceObject.GetComponent<DiceNum>().diceSides =4;
        GameManager.Instance.movePlayer2.diceObject.GetComponent<DiceNum>().diceSides =6;
        p1dado4.SetActive(true);
        p2dado6.SetActive(true);
        p1dado6.SetActive(false);
        p2dado4.SetActive(false);

        PlayerPrefs.SetString("1D6","P2");
        }
        if(alreadyChange==false && PlayerPrefs.GetString("1D6") == "P2")
        {
        alreadyChange =true;
        GameManager.Instance.movePlayer1.diceObject.GetComponent<DiceNum>().diceSides =6;
        GameManager.Instance.movePlayer2.diceObject.GetComponent<DiceNum>().diceSides =4;
        p1dado4.SetActive(false);
        p2dado6.SetActive(false);
        p1dado6.SetActive(true);
        p2dado4.SetActive(true);
        PlayerPrefs.SetString("1D6","P1");
        }
        alreadyChange =false;
    }

  
}

}
