using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CasillaNeutral : MonoBehaviour
{
     private bool activeDefault;
    private GameManager gameM;
    private Turnos turnoM;

    private void Start()
    {
        gameM = GameObject.Find("GameManager").GetComponent<GameManager>();
        turnoM = gameM.gameObject.GetComponent<Turnos>();
    }

    private void OnTriggerEnter(Collider other)
    {
        activeDefault = true;
    }

    private void OnTriggerStay(Collider other)
    {

        if (activeDefault && !gameM.movePlayer1.isOnRoute && !gameM.movePlayer2.isOnRoute)
        {
            int player = other.GetComponent<MovePlayer>().playerNum;
            if (player == 1)
            {
                turnoM.finishP1 = true;
            }
            else
            {
                turnoM.finishP2 = true;
            }

            activeDefault = false;
        }
    }
}
