using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventSaltarTurno : MonoBehaviour
{
    private GameManager gameM;
    private Turnos turnoM;
    private bool activeEvent, activeDefault;

    void Start()
    {
        gameM = GameObject.Find("GameManager").GetComponent<GameManager>();
        turnoM = gameM.gameObject.GetComponent<Turnos>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(activeEvent) activeDefault = true;
    }

    private void OnTriggerStay(Collider other)
    {
        if (!activeEvent)
        {
            if (other.CompareTag("Player"))
            {
                if (!gameM.movePlayer1.isOnRoute && !gameM.movePlayer2.isOnRoute)
                {
                    int player = other.GetComponent<MovePlayer>().playerNum;
                    print(player);
                    if (player == 1)
                    {
                        turnoM.waitP1 = true;
                        turnoM.finishP1 = true;
                    }
                    else
                    {
                        turnoM.waitP2 = true;
                        turnoM.finishP2 = true;
                    }
                    activeEvent = true;
                }
            }
        }
        else if (activeDefault && !gameM.movePlayer1.isOnRoute && !gameM.movePlayer2.isOnRoute && activeEvent)
        {
            int player = other.GetComponent<MovePlayer>().playerNum;
            if (player == 1)
            {
                turnoM.finishP1 = true;
            }
            else
            {
                turnoM.finishP2 = true;
            }

            activeDefault = false;
        }
    }
}
