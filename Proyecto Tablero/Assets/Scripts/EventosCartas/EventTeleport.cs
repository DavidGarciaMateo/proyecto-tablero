using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EventTeleport : MonoBehaviour
{
 [SerializeField] private Transform player1;
    [SerializeField] private Transform player2;

    private MovePlayer player;

    private Vector3 pl1LastPos;
    private Vector3 pl2LastPos;

    private bool alreadyUsed = false;
    [SerializeField] private float timeToWaitEvent = 3;
    [SerializeField]private bool bloockingEvent =false; /*Booleana para bloquear el evento en funcion del tiempo */

private void Start() {
  if(!PlayerPrefs.HasKey("Active"+gameObject.name))
  {
    PlayerPrefs.SetInt("Active"+gameObject.name,1);
  }

}
private void OnTriggerEnter(Collider other) {
  alreadyUsed = false;
}
    private void OnTriggerStay(Collider other)
    {
        if(PlayerPrefs.GetInt("Active"+gameObject.name)==1) /*asegura que la casilla solo se pueda usar una vez, valorar si hace falta */
        {
            //Inicia la detecciond  en funcion de si ya se ha utilizado antes y cuando los dos players han finalizado la ruta,
            if(!alreadyUsed && player1.gameObject.GetComponent<MovePlayer>().isOnRoute==false && player2.gameObject.GetComponent<MovePlayer>().isOnRoute == false)
            {
                Debug.Log("A");
                PlayerPrefs.SetInt("Active"+gameObject.name,0);
                alreadyUsed = true; /*Se pone a true para evitar q caigan en un bucle cambiando las posiciones */
                if (other.CompareTag("Player"))
                {
                    player = other.GetComponent<MovePlayer>();
                    StartCoroutine(WaitToTeleport());
                }
            } 

        }
    }
    private IEnumerator WaitToTeleport()
    {
      yield return new WaitForSeconds(timeToWaitEvent); /*Determianr el tiempo que tienen los players para bloquear el evento */
        if (bloockingEvent == false)/*Booleana que permie detener el evento */
        {

            //intercambio de rutas en funciond de lo guardado anteriormente
            player1.gameObject.GetComponent<MovePlayer>().parent = GameObject.Find(PlayerPrefs.GetString("RutaPL2"));
            player2.gameObject.GetComponent<MovePlayer>().parent = GameObject.Find(PlayerPrefs.GetString("RutaPL1"));

            //actualizar los playerpref con las rutas actuales de cada Player
            PlayerPrefs.SetString("RutaPL1", player1.gameObject.GetComponent<MovePlayer>().parent.name);
            PlayerPrefs.SetString("RutaPL2", player2.gameObject.GetComponent<MovePlayer>().parent.name);

            //intercambio de posiciones en las casillas
            player1.gameObject.GetComponent<MovePlayer>().PlayerPos = PlayerPrefs.GetInt("PosPL2");
            player2.gameObject.GetComponent<MovePlayer>().PlayerPos = PlayerPrefs.GetInt("PosPL1");

            //actualizacionde del playerpref
            PlayerPrefs.SetInt("Pos1", player1.gameObject.GetComponent<MovePlayer>().PlayerPos);
            PlayerPrefs.SetInt("Pos2", player2.gameObject.GetComponent<MovePlayer>().PlayerPos);

            player1.gameObject.GetComponent<NavMeshAgent>().enabled=false;
            player2.gameObject.GetComponent<NavMeshAgent>().enabled=false;
            //Guardado de posiciones para su posterior asignacion
            pl1LastPos = player1.position;
            pl2LastPos = player2.position;

            //Cambio de posciones
            player1.position = pl2LastPos;
            player2.position = pl1LastPos;
            player1.gameObject.GetComponent<NavMeshAgent>().enabled=true;
            player2.gameObject.GetComponent<NavMeshAgent>().enabled=true;

            //vaciado de la lista de ruta anterior e incorporacion de la nueva ruta
            player1.gameObject.GetComponent<MovePlayer>().Teleport = true;
            player2.gameObject.GetComponent<MovePlayer>().Teleport = true;

            //actualizar posiciones del playerpref tras el cambio de posicion
            PlayerPrefs.SetInt("PosPL1", GameManager.Instance.movePlayer1.PlayerPos);
            PlayerPrefs.SetFloat("PosX1", GameManager.Instance.movePlayer1.gameObject.transform.position.x);
            PlayerPrefs.SetFloat("PosY1", GameManager.Instance.movePlayer1.gameObject.transform.position.y);
            PlayerPrefs.SetFloat("PosZ1", GameManager.Instance.movePlayer1.gameObject.transform.position.z);

            PlayerPrefs.SetInt("PosPL2", GameManager.Instance.movePlayer2.PlayerPos);
            PlayerPrefs.SetFloat("PosX2", GameManager.Instance.movePlayer2.gameObject.transform.position.x);
            PlayerPrefs.SetFloat("PosY2", GameManager.Instance.movePlayer2.gameObject.transform.position.y);
            PlayerPrefs.SetFloat("PosZ2", GameManager.Instance.movePlayer2.gameObject.transform.position.z);

            yield return new WaitForSeconds(3f);

            PlayerPrefs.SetInt("TurnCountP1",3);
            PlayerPrefs.SetInt("TurnCountP2",3);

            if(player.playerNum == 1)
            {
                GameManager.Instance.turnos.finishP1 = true;
            }
            else if(player.playerNum == 2)
            {
                GameManager.Instance.turnos.finishP2 = true;
            }

            //GameManager.Instance.turnos.Turno();
        }

    }
}
