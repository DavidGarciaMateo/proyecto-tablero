using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
   
    public static GameManager Instance;

    [Header("Evento Cambio Posición")]
    /*public GameObject currentRutePj1;
    public GameObject currentRutePj2;*/
    public Vector3 positionPL1;
    public Vector3 positionPL2;

    [Header("evento Intercambio dados")]
    public CambioDados cambioDados;
    public bool evenetDices = false; /*La carta de cambio de dado deberia pasar la boleana a true*/
    public MovePlayer movePlayer1;
    public MovePlayer movePlayer2;
  //  public GameObject dice1D6;
  //  public GameObject dice1D4;
    [SerializeField] private GameObject p1dado6;
    [SerializeField] private GameObject p1dado4;
    [SerializeField] private GameObject p2dado6;
    [SerializeField] private GameObject p2dado4;

    private string preferencia;

    public CardCanvasManager cardCanvasManager;

    public CasillaDetection casillaDetection;

    public Turnos turnos;
    public bool resetPrefab;
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
 //reseta playerpref

    if(!PlayerPrefs.HasKey("InitialGame"))
    {
        PlayerPrefs.SetInt("InitialGame",0);
    }
    else
    {
        PlayerPrefs.SetInt("InitialGame",1);
    }

    if(PlayerPrefs.GetInt("InitialGame")==0)
    {
        casillaDetection.ChooseMinigame();
    }

         if(resetPrefab)
        {
            PlayerPrefs.DeleteAll();
        }
        //Lectura quien tiene 1D6
        if (!PlayerPrefs.HasKey("1D6"))
        {
            PlayerPrefs.SetString("1D6", "P1");
        }


        if (PlayerPrefs.GetString("1D6") == "P1")
        {
           // movePlayer1.diceObject = dice1D6;
           // movePlayer2.diceObject = dice1D4;
        movePlayer1.diceObject.GetComponent<DiceNum>().diceSides =6;
        movePlayer2.diceObject.GetComponent<DiceNum>().diceSides =4;
        p1dado4.SetActive(false);
        p1dado6.SetActive(true);
        p2dado6.SetActive(false);
        p2dado4.SetActive(true);

        }
        if (PlayerPrefs.GetString("1D6") == "P2")
        {
            //movePlayer1.diceObject = dice1D4;
           // movePlayer2.diceObject = dice1D6;
            movePlayer1.diceObject.GetComponent<DiceNum>().diceSides =4;
            movePlayer2.diceObject.GetComponent<DiceNum>().diceSides =6;
            p1dado4.SetActive(true);
            p1dado6.SetActive(false);
            p2dado6.SetActive(true);
            p2dado4.SetActive(false);

        }
       

        //Lectura posicion del Player1
        if (!PlayerPrefs.HasKey("PosX1"))
        {
            PlayerPrefs.SetFloat("PosX1", movePlayer1.gameObject.transform.position.x);
        }
        else
        {
            positionPL1.x = PlayerPrefs.GetFloat("PosX1");
        }

        if (!PlayerPrefs.HasKey("PosY1"))
        {
            PlayerPrefs.SetFloat("PosY1", movePlayer1.transform.position.y);
        }
        else
        {
            positionPL1.y = PlayerPrefs.GetFloat("PosY1");
        }

        if (!PlayerPrefs.HasKey("PosZ1"))
        {
            PlayerPrefs.SetFloat("PosZ1", movePlayer1.transform.position.z);
        }
        else
        {
            positionPL1.z = PlayerPrefs.GetFloat("PosZ1");
        movePlayer1.gameObject.transform.position = positionPL1;

        }

        //Lectura posicon Player2
        if (!PlayerPrefs.HasKey("PosX2"))
        {
            PlayerPrefs.SetFloat("PosX2", movePlayer2.transform.position.x);
        }
        else
        {
            positionPL2.x = PlayerPrefs.GetFloat("PosX2");
        }

        if (!PlayerPrefs.HasKey("PosY2"))
        {
            PlayerPrefs.SetFloat("PosY2", movePlayer2.transform.position.y);
        }
        else
        {
            positionPL2.y = PlayerPrefs.GetFloat("PosY2");
        }

        if (!PlayerPrefs.HasKey("PosZ2"))
        {
            PlayerPrefs.SetFloat("PosZ2", movePlayer2.transform.position.z);
        }
        else
        {
            positionPL2.z = PlayerPrefs.GetFloat("PosZ2");
            movePlayer2.gameObject.transform.position = positionPL2;

        }

        //Para controlar la prefrencia de turno de cartas
        if (!PlayerPrefs.HasKey("Preferencia"))
        {
            PlayerPrefs.SetString("Preferencia", "P1");
        }
        else
        {
            preferencia = PlayerPrefs.GetString("Preferencia");
        }
        //Lectura ruta Player1
        if (!PlayerPrefs.HasKey("RutaPL1"))
        {
            PlayerPrefs.SetString("RutaPL1", movePlayer1.parent.name);
        }
        else
        {
            movePlayer1.parent = GameObject.Find(PlayerPrefs.GetString("RutaPL1"));
        }

        //Lectura ruta Player2
        if (!PlayerPrefs.HasKey("RutaPL2"))
        {
            PlayerPrefs.SetString("RutaPL2", movePlayer2.parent.name);
        }
        else
        {
            movePlayer2.parent = GameObject.Find(PlayerPrefs.GetString("RutaPL2"));
        }   
}

    public void LoadCards(){

        
        for (int i = 0; i < PlayerPrefs.GetInt("sizeCardsP2");i++){

            cardCanvasManager.GenerateCard2((GameObject)Resources.Load(PlayerPrefs.GetString("Cards2_"+i)));

        }

        for (int i = 0; i < PlayerPrefs.GetInt("sizeCardsP1");i++){

            cardCanvasManager.GenerateCard((GameObject)Resources.Load(PlayerPrefs.GetString("Cards1_"+i)));

        }
    }

    public void SafeCards(){

        PlayerPrefs.SetInt("sizeCardsP1",cardCanvasManager.player1Cards.Count);
        PlayerPrefs.SetInt("sizeCardsP2",cardCanvasManager.player2Cards.Count);

        for (int i = 0; i < cardCanvasManager.player1Cards.Count; i++)
        {
    
            PlayerPrefs.SetString("Cards1_" + i, cardCanvasManager.player1Cards[i].name);
            
        }

        for (int i = 0; i < cardCanvasManager.player2Cards.Count; i++)
        {
           PlayerPrefs.SetString("Cards2_" + i, cardCanvasManager.player2Cards[i].name);
            
        }
        
    }

    private void Start()
    { 
      /*  Vector3 pos1= movePlayer1.gameObject.transform.position;
        Vector3 pos2 = movePlayer2.gameObject.transform.position;

        movePlayer1.gameObject.transform.position = pos2;
        movePlayer2.gameObject.transform.position= pos1;*/

       
        turnos= GetComponent<Turnos>();
         if (!PlayerPrefs.HasKey("PosPL1"))
        {
            PlayerPrefs.SetInt("PosPL1", movePlayer1.PlayerPos);
        }
        else
        {
            movePlayer1.PlayerPos = PlayerPrefs.GetInt("PosPL1");
        }

        //Lectura casilla del PLayer2
        if (!PlayerPrefs.HasKey("PosPL2"))
        {
            PlayerPrefs.SetInt("PosPL2", movePlayer2.PlayerPos);
        }
        else
        {
            Debug.Log("Entro"+PlayerPrefs.GetInt("PosPL2"));
            movePlayer2.PlayerPos = PlayerPrefs.GetInt("PosPL2");
        }

        LoadCards();      
    }

private void LateUpdate() {
        //funcion para cambiar dados, se activacon una booleana
        cambioDados.ChangeDices();

        //Almacena la poscion de los players miestras estan en ruta
      if(movePlayer1.isOnRoute)
      {
        PlayerPrefs.SetInt("PosPL1", movePlayer1.PlayerPos);
        PlayerPrefs.SetFloat("PosX1",movePlayer1.gameObject.transform.position.x);
        PlayerPrefs.SetFloat("PosY1",movePlayer1.gameObject.transform.position.y);
        PlayerPrefs.SetFloat("PosZ1",movePlayer1.gameObject.transform.position.z);
      }
       if(movePlayer2.isOnRoute)
      {
        PlayerPrefs.SetInt("PosPL2", movePlayer2.PlayerPos);
        PlayerPrefs.SetFloat("PosX2",movePlayer2.gameObject.transform.position.x);
        PlayerPrefs.SetFloat("PosY2",movePlayer2.gameObject.transform.position.y);
        PlayerPrefs.SetFloat("PosZ2",movePlayer2.gameObject.transform.position.z);
      }

      if(PlayerPrefs.GetInt("TurnCountP1")==3 && PlayerPrefs.GetInt("TurnCountP2") == 3 ){

        PlayerPrefs.SetInt("TurnCountP1",0);
        PlayerPrefs.SetInt("TurnCountP2",0);

        turnos.Turno();
      }
        if(movePlayer1.isStart && movePlayer2.isStart)
        {
            movePlayer1.NextPos();
            movePlayer2.NextPos();

            movePlayer1.isStart = false;
            movePlayer2.isStart = false;
        }

    }
private void EstadosTurno()
{
    switch ( PlayerPrefs.GetInt("TurnCountP1"))
    {

        case 0:
        {
            turnos.Turno();
            PlayerPrefs.SetInt("TurnCountP1",1);

            break;
        }
        case 1:
        {

            break;
        }
        case 2:
        {
            break;
        }
        case 3:
        {
            break;
        }
    }
}
}
