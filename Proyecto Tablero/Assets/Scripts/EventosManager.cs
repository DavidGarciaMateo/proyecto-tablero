using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventosManager : MonoBehaviour
{

     public List<GameObject> playerCardsAUX;
    public CardCanvasManager cardCanvasManager;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Retroceso(){

    }

    public void Avanza(){
        
    }

    public void ImparRetrocedeParAvanza(){
        
    }

    public void IntercambiaPosiciones(){
        
    }

    public void CambiarCartas(){

        playerCardsAUX = cardCanvasManager.player1Cards;
        cardCanvasManager.player1Cards = cardCanvasManager.player2Cards;
        cardCanvasManager.player2Cards = playerCardsAUX;

        

    }

    public void PerdidaCartas(){
        
    }

    public void PerderTurno(){
        
    }

    public void ParAvanzasImparNo(){
        
    }
}
