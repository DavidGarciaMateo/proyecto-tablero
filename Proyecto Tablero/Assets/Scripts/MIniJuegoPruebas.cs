using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class MIniJuegoPruebas : MonoBehaviour
{

    public void winP1()
    {
        PlayerPrefs.SetString("Preferencia", "P1");
        PlayerPrefs.SetString("1D6", "P1");

        SceneManager.LoadScene("Tablero");
    }
    public void winP2()
    {
        PlayerPrefs.SetString("Preferencia", "P2");
        PlayerPrefs.SetString("1D6", "P2");
        SceneManager.LoadScene("Tablero");

    }



    // Start is called before the first frame update
    void Start()
    {
        PlayerPrefs.DeleteAll();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
