using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerSelector : MonoBehaviour
{
    [SerializeField] private Transform[] positions;
    [SerializeField] private SelectionMenuController controller;
    private int actualPos;



private void Start() 
{
    transform.position = positions[1].position;
    actualPos =1;
}

//Desplazamiento derecha
private void OnGoRight(InputValue value)
{ 
    if(value.isPressed && actualPos!= positions.Length-1)
    {
        transform.position = positions[(actualPos+1)].position;
        actualPos++;
    }
}
//Desplazamiento izquierda
private void OnGoLeft(InputValue value)
{
    if(value.isPressed && actualPos!=0)
    {
        transform.position = positions[(actualPos-1)].position;
        actualPos--;
    }
}
//Selececcionar uno de los 2 players
private void OnSelectPl(InputValue value)
{
    if(value.isPressed)
    {
        if(Vector3.Distance(transform.position, positions[0].position)< 1 && controller.Pl2Selected == false)
        {
            controller.Pl2Selected = true;
            GetComponent<PlayerInput>().enabled = false;
            controller.Pl2Animator.SetTrigger("Select");

            //guardamos la seleccion en un playepref para recordarlo en otras escenas
            PlayerPrefs.SetInt(gameObject.name,2);
        }
        if(Vector3.Distance(transform.position,positions[2].position) < 1 && controller.Pl1Selected == false)
        {
            controller.Pl1Selected = true;
            GetComponent<PlayerInput>().enabled = false;
            controller.Pl1Animator.SetTrigger("Select");

            //guardamos la seleccion en un playepref para recordarlo en otras escenas
            PlayerPrefs.SetInt(gameObject.name,2);
        }
        if(controller.Pl1Selected == true && controller.Pl2Selected == true)
        {
            StartCoroutine(controller.SceneChange());
        }
    }

}
}
