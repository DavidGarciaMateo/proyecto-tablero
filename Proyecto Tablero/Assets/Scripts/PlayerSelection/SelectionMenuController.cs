using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SelectionMenuController : MonoBehaviour
{
//boleanas que controlan que los dos jugadores no puedas escoger el mismo player
    [SerializeField] private bool pl1Selected;
    [SerializeField] private bool pl2Selected;

    //Animator para las poses de selección
    [SerializeField] private Animator pl1Animator;
    [SerializeField] private Animator pl2Animator;

    public bool Pl1Selected { get => pl1Selected; set => pl1Selected = value; }
    public bool Pl2Selected { get => pl2Selected; set => pl2Selected = value; }
    public Animator Pl1Animator { get => pl1Animator; set => pl1Animator = value; }
    public Animator Pl2Animator { get => pl2Animator; set => pl2Animator = value; }

    public IEnumerator SceneChange()
    {
    yield return new WaitForSeconds(3);
    //Poner las ecena a cargar
        //SceneManager.LoadScene(2);
    }

    public void Botonaso(){
        SceneManager.LoadScene("Tablero");
    }
}
