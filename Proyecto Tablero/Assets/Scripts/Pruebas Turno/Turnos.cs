using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Turnos : MonoBehaviour
{

    public string preferencia = "P1";
    public float timeToPlay = 0.25f;
    public Slider timerP1, timerP2;

    private bool bothFinish = false;
    

    public static CardCanvasManager cardCanvasManager;

    public GameObject panelCardsP1Disabled, panelCardsP2Disabled;

    public bool cardPhase1, cardPhase2, waitP1, waitP2, finishP1, finishP2;

    public MovePlayer movePlayer1, movePlayer2;
    // Start is called before the first frame update
    void Start()
    {   
        movePlayer1 = GameObject.Find("Player1").GetComponent<MovePlayer>();
        movePlayer2 = GameObject.Find("Player2").GetComponent<MovePlayer>();
        Turno();
        
    }

    // Update is called once per frame
    void Update()
    {   
        //cardPhase1 = cardCanvasManager.cardPhase1;
        //cardPhase2 = cardCanvasManager.cardPhase2;
        if(bothFinish == true){
            Turno();
        }
        if (finishP1 && finishP2)
        {
            Turno();
            finishP2 = false;
            finishP1 = false;
        }
    }

    public void Turno(){
        StartCoroutine(WaitToTurno());
       /* timerP1.value =1;
        timerP2.value =1;
        preferencia = PlayerPrefs.GetString("Preferencia");
        bothFinish = false;
        //Si la preferencia la tiene el P1, este empezara jugando y su barra de tiempo de juego con sus cartas empezara a bajar progresivamente.
        if(PlayerPrefs.GetString("Preferencia") == "P1"){
            StartCoroutine(DecreseSlider(timerP1, timerP2, panelCardsP1Disabled, panelCardsP2Disabled, cardPhase1, cardPhase2));

        }else{

            StartCoroutine(DecreseSlider(timerP2, timerP1 , panelCardsP2Disabled, panelCardsP1Disabled, cardPhase2, cardPhase1));

        }

        PlayerPrefs.SetInt("TurnCountP1",1);
        PlayerPrefs.SetInt("TurnCountP2",1);

        movePlayer1.NewDiceTurn();
        movePlayer2.NewDiceTurn(); */   
        
    }

    
private IEnumerator WaitToTurno()
{
    yield return new WaitForSeconds (2);
 timerP1.value =1;
        timerP2.value =1;
        preferencia = PlayerPrefs.GetString("Preferencia");
        bothFinish = false;
        //Si la preferencia la tiene el P1, este empezara jugando y su barra de tiempo de juego con sus cartas empezara a bajar progresivamente.
        if(PlayerPrefs.GetString("Preferencia") == "P1"){
            StartCoroutine(DecreseSlider(timerP1, timerP2, panelCardsP1Disabled, panelCardsP2Disabled, cardPhase1, cardPhase2));

        }else{

            StartCoroutine(DecreseSlider(timerP2, timerP1 , panelCardsP2Disabled, panelCardsP1Disabled, cardPhase2, cardPhase1));

        }

        PlayerPrefs.SetInt("TurnCountP1",1);
        PlayerPrefs.SetInt("TurnCountP2",1);

       /* movePlayer1.NewDiceTurn();
        movePlayer2.NewDiceTurn();    */
        if (!waitP1)
        {
            movePlayer1.NewDiceTurn();
        }
        else
        {
            waitP1 = false;
            movePlayer1.isStart = true;
        }

        if (!waitP2)
        {
            movePlayer2.NewDiceTurn();
        }
        else
        {
            waitP2 = false;
            movePlayer2.isStart = true;
        }

}


   public IEnumerator DecreseSlider(Slider sliderFirst, Slider sliderSecond, GameObject panelDisableFirst , GameObject panelDisableSecond, bool cardPhase1, bool cardPhase2)
    {
     if (sliderFirst != null)
     {

         //Cambiar el numero del final para que vaya mas rapido o lento!!!
         float timeSlice = (sliderFirst.value / 30);
         while (sliderFirst.value >= 0)
         {
             sliderFirst.value -= timeSlice;
             yield return new WaitForSeconds(timeToPlay);
             if (sliderFirst.value <= 0){
                
                cardPhase1 = false;
                yield return  StartCoroutine(DecreseSlider2(sliderFirst, sliderSecond, panelDisableFirst, panelDisableSecond, cardPhase1, cardPhase2));
             }
                 
         }
     }
     yield return null;
     }

 public IEnumerator DecreseSlider2(Slider sliderFirst, Slider sliderSecond, GameObject panelDisableFirst , GameObject panelDisableSecond, bool cardPhase1, bool cardPhase2)
 {
     if (sliderSecond != null)
     {
         float timeSlice = (sliderSecond.value / 30);
         while (sliderSecond.value >= 0)
         {
             sliderSecond.value -= timeSlice;
             yield return new WaitForSeconds(timeToPlay);
             if (sliderSecond.value <= 0){
                 cardPhase2 = false;
                
                 break;
             }
                 
         }
     }
     yield return null;
  }
}
