using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class CardCanvasManager : MonoBehaviour
{
    public List<GameObject> player1Cards, player2Cards;

    private GameObject newCard1, newCard2;

    public Transform CardParent1, CardParent2;
    public Canvas canvas;

    private Vector3 cardY;

    public Color colorNegro, colorNormal;

    public bool cardPhase1, cardPhase2, discardPhase1, discardPhase2;
    private int currentCard1, currentCard2;

    public GameManager gameManager;



    private bool canvasDetroyed;
    // Start is called before the first frame update

    void Start()
    {
        cardPhase1 = true;
        cardPhase2 = true;          
    }


    public void CambiarEscena()
    {
        gameManager.SafeCards();
        SceneManager.LoadScene("PlayerSelector");       
    }

    void DesactivarCanvas(){
        canvas.gameObject.SetActive(false);
    }

    

    void Update()
    {
        Player1CardController();
        Player2CardController();

        if(cardPhase1 == false){
            for (int i = 0; i < player1Cards.Count; i++)
            {
                player1Cards[i].GetComponent<Image>().color =  colorNegro;
                
            }

        }else{
            for (int i = 0; i < player1Cards.Count; i++)
            {
                player1Cards[i].GetComponent<Image>().color =  colorNormal;
            }
        }

        if(cardPhase2 == false){
            for (int i = 0; i < player2Cards.Count; i++)
            {
                player2Cards[i].GetComponent<Image>().color =  colorNegro;
            }

        }else{
            for (int i = 0; i < player2Cards.Count; i++)
            {
                player2Cards[i].GetComponent<Image>().color =  colorNormal;
            }
        }
    }
    private IEnumerator WaitForCardAnimation()
    {
        player1Cards.Add(newCard1);

        if (player1Cards.Count == 6)
        {
            cardPhase1 = false;
            discardPhase1 = true;
        }
        
        for (int i = 0; i < player1Cards.Count; i++)
        {
            if (i == 0)
            {
                newCard1.transform.localPosition = Vector3.Lerp(newCard1.transform.localPosition, CardParent1.transform.localPosition, 1);
                cardY = player1Cards[i].transform.localPosition;
            }
            if(i != 0)
            {
                Vector3 newPos = player1Cards[i - 1].transform.localPosition;
                newPos.x += 45;
                player1Cards[i].transform.localPosition = newPos;
            }
        }

        yield return new WaitForSeconds(0.1f);
    }
    private IEnumerator WaitForCardAnimation2()
    {
        player2Cards.Add(newCard2);

        if (player2Cards.Count == 6)
        {
            cardPhase2 = false;
            discardPhase2 = true;
        }

        for (int i = 0; i < player2Cards.Count; i++)
        {
            if (i == 0)
            {
                newCard2.transform.localPosition = Vector3.Lerp(newCard2.transform.localPosition, CardParent2.transform.localPosition, 1);
                cardY = player2Cards[i].transform.localPosition;
            }
            if(i != 0)
            {
                Vector3 newPos = player2Cards[i - 1].transform.localPosition;
                newPos.x -= 45;
                player2Cards[i].transform.localPosition = newPos;
            }
        }

        yield return new WaitForSeconds(0.1f);
    }
    private void Player1CardController()
    {
        if (cardPhase1)
        {
            if (player1Cards.Count > 1)
            {
                 if (Input.GetAxis("Xaxis") == -1)
                {
                    currentCard1 -= 1;
                    if (currentCard1 < 0)
                    {
                        currentCard1 = player1Cards.Count - 1;
                    }
                    for (int i = 0; i < player1Cards.Count; i++)
                    {
                        if (i == currentCard1)
                        {
                            player1Cards[currentCard1].transform.localPosition = new Vector3(player1Cards[currentCard1].transform.localPosition.x, player1Cards[currentCard1].transform.localPosition.y + 25, player1Cards[currentCard1].transform.localPosition.z);
                        }
                        else
                        {
                            player1Cards[i].transform.localPosition = new Vector3(player1Cards[i].transform.localPosition.x, cardY.y, player1Cards[i].transform.localPosition.z);
                        }
                    }
                }
                 if (Input.GetAxis("Xaxis") == 1)
                {
                    currentCard1 += 1;
                    if (currentCard1 == player1Cards.Count)
                    {
                        currentCard1 = 0;
                    }
                    for (int i = 0; i < player1Cards.Count; i++)
                    {
                        if (i == currentCard1)
                        {
                            player1Cards[currentCard1].transform.localPosition = new Vector3(player1Cards[currentCard1].transform.localPosition.x, player1Cards[currentCard1].transform.localPosition.y + 25, player1Cards[currentCard1].transform.localPosition.z);
                        }
                        else
                        {
                            player1Cards[i].transform.localPosition = new Vector3(player1Cards[i].transform.localPosition.x, cardY.y, player1Cards[i].transform.localPosition.z);
                        }
                    }
                }
            }
            else
            {
                currentCard1 = 0;
            }
            if (Input.GetKeyDown(KeyCode.Joystick1Button3))
            {
                //IMPORTANTE ESTA
                player1Cards[currentCard1].GetComponent<CartasUsos>().UsarCard(2);

                Destroy(player1Cards[currentCard1], 0.1f);
                player1Cards.Remove(player1Cards[currentCard1]);
                for (int i = 0; i < player1Cards.Count; i++)
                {
                    if (i == 0)
                    {
                        player1Cards[i].transform.localPosition = Vector3.Lerp(player1Cards[i].transform.localPosition, CardParent1.transform.localPosition, 100);
                    }
                    if (i != 0)
                    {
                        Vector3 newPos = player1Cards[i - 1].transform.localPosition;
                        newPos.x += 75;
                        player1Cards[i].transform.localPosition = newPos;
                    }
                }
                currentCard1 = 0;
            }

        }
        else if (discardPhase1)
        {
            if (player1Cards.Count > 1)
            {
                if (Input.GetAxis("Xaxis") == -1)
                {
                    currentCard1 -= 1;
                    if (currentCard1 < 0)
                    {
                        currentCard1 = player1Cards.Count - 1;
                    }
                    for (int i = 0; i < player1Cards.Count; i++)
                    {
                        if (i == currentCard1)
                        {
                            player1Cards[currentCard1].transform.localPosition = new Vector3(player1Cards[currentCard1].transform.localPosition.x, player1Cards[currentCard1].transform.localPosition.y + 25, player1Cards[currentCard1].transform.localPosition.z);
                        }
                        else
                        {
                            player1Cards[i].transform.localPosition = new Vector3(player1Cards[i].transform.localPosition.x, cardY.y, player1Cards[i].transform.localPosition.z);
                        }
                    }
                }
                if (Input.GetAxis("Xaxis") == 1)
                {
                    currentCard1 += 1;
                    if (currentCard1 == player1Cards.Count)
                    {
                        currentCard1 = 0;
                    }
                    for (int i = 0; i < player1Cards.Count; i++)
                    {
                        if (i == currentCard1)
                        {
                            player1Cards[currentCard1].transform.localPosition = new Vector3(player1Cards[currentCard1].transform.localPosition.x, player1Cards[currentCard1].transform.localPosition.y + 25, player1Cards[currentCard1].transform.localPosition.z);
                        }
                        else
                        {
                            player1Cards[i].transform.localPosition = new Vector3(player1Cards[i].transform.localPosition.x, cardY.y, player1Cards[i].transform.localPosition.z);
                        }
                    }
                }
            }
            else
            {
                currentCard1 = 0;
            }
            if (Input.GetKeyDown(KeyCode.Joystick1Button3))
            {
                Destroy(player1Cards[currentCard1], 0.1f);
                player1Cards.Remove(player1Cards[currentCard1]);
                for (int i = 0; i < player1Cards.Count; i++)
                {
                    if (i == 0)
                    {
                        player1Cards[i].transform.localPosition = Vector3.Lerp(player1Cards[i].transform.localPosition, CardParent1.transform.localPosition, 100);
                    }
                    if (i != 0)
                    {
                        Vector3 newPos = player1Cards[i - 1].transform.localPosition;
                        newPos.x += 75;
                        player1Cards[i].transform.localPosition = newPos;
                    }
                }
                currentCard1 = 0;
                discardPhase1 =false;
            }
        }
    }
    private void Player2CardController()
    {
        if (cardPhase2)
        {
            if (player2Cards.Count > 1)
            {
                if (Input.GetKeyDown(KeyCode.LeftArrow))
                {
                    currentCard2 -= 1;
                    if (currentCard2 < 0)
                    {
                        currentCard2 = player2Cards.Count - 1;
                    }
                    for (int i = 0; i < player2Cards.Count; i++)
                    {
                        if (i == currentCard2)
                        {
                            player2Cards[currentCard2].transform.localPosition = new Vector3(player2Cards[currentCard2].transform.localPosition.x, player2Cards[currentCard2].transform.localPosition.y + 25, player2Cards[currentCard2].transform.localPosition.z);
                        }
                        else
                        {
                            player2Cards[i].transform.localPosition = new Vector3(player2Cards[i].transform.localPosition.x, cardY.y, player2Cards[i].transform.localPosition.z);
                        }
                    }
                }
                if (Input.GetKeyDown(KeyCode.RightArrow))
                {
                    currentCard2 += 1;
                    if (currentCard2 == player2Cards.Count)
                    {
                        currentCard2 = 0;
                    }
                    for (int i = 0; i < player2Cards.Count; i++)
                    {
                        if (i == currentCard2)
                        {
                            player2Cards[currentCard2].transform.localPosition = new Vector3(player2Cards[currentCard2].transform.localPosition.x, player2Cards[currentCard2].transform.localPosition.y + 25, player2Cards[currentCard2].transform.localPosition.z);
                        }
                        else
                        {
                            player2Cards[i].transform.localPosition = new Vector3(player2Cards[i].transform.localPosition.x, cardY.y, player2Cards[i].transform.localPosition.z);
                        }
                    }
                }

            }
            else
            {
                currentCard2 = 0;
            }
            if (Input.GetKeyDown(KeyCode.Q))
            {
                //IMPORTANTE ESTA TAMBIEN
                player2Cards[currentCard2].GetComponent<CartasUsos>().UsarCard(1);
                Destroy(player2Cards[currentCard2], 0.1f);
                player2Cards.Remove(player2Cards[currentCard2]);
                for (int i = 0; i < player2Cards.Count; i++)
                {
                    if (i == 0)
                    {
                        player2Cards[i].transform.localPosition = Vector3.Lerp(player2Cards[i].transform.localPosition, CardParent2.transform.localPosition, 100);
                    }
                    if (i != 0)
                    {
                        Vector3 newPos = player2Cards[i - 1].transform.localPosition;
                        newPos.x += 75;
                        player2Cards[i].transform.localPosition = newPos;
                    }
                }
                currentCard2 = 0;
            }

        }
        else if (discardPhase2)
        {
            if (player2Cards.Count > 1)
            {
                if (Input.GetKeyDown(KeyCode.LeftArrow))
                {
                    currentCard2 -= 1;
                    if (currentCard2 < 0)
                    {
                        currentCard2 = player2Cards.Count - 1;
                    }
                    for (int i = 0; i < player2Cards.Count; i++)
                    {
                        if (i == currentCard2)
                        {
                            player2Cards[currentCard2].transform.localPosition = new Vector3(player2Cards[currentCard2].transform.localPosition.x, player2Cards[currentCard2].transform.localPosition.y + 25, player2Cards[currentCard2].transform.localPosition.z);
                        }
                        else
                        {
                            player2Cards[i].transform.localPosition = new Vector3(player2Cards[i].transform.localPosition.x, cardY.y, player2Cards[i].transform.localPosition.z);
                        }
                    }
                }
                if (Input.GetKeyDown(KeyCode.RightArrow))
                {
                    currentCard2 += 1;
                    if (currentCard2 == player2Cards.Count)
                    {
                        currentCard2 = 0;
                    }
                    for (int i = 0; i < player2Cards.Count; i++)
                    {
                        if (i == currentCard2)
                        {
                            player2Cards[currentCard2].transform.localPosition = new Vector3(player2Cards[currentCard2].transform.localPosition.x, player2Cards[currentCard2].transform.localPosition.y + 25, player2Cards[currentCard2].transform.localPosition.z);
                        }
                        else
                        {
                            player2Cards[i].transform.localPosition = new Vector3(player2Cards[i].transform.localPosition.x, cardY.y, player2Cards[i].transform.localPosition.z);
                        }
                    }
                }
            }
            else
            {
                currentCard2 = 0;
            }
            if (Input.GetKeyDown(KeyCode.Q))
            {
                Destroy(player2Cards[currentCard2], 0.1f);
                player2Cards.Remove(player2Cards[currentCard2]);
                for (int i = 0; i < player2Cards.Count; i++)
                {
                    if (i == 0)
                    {
                        player2Cards[i].transform.localPosition = Vector3.Lerp(player2Cards[i].transform.localPosition, CardParent2.transform.localPosition, 100);
                    }
                    if (i != 0)
                    {
                        Vector3 newPos = player2Cards[i - 1].transform.localPosition;
                        newPos.x += 75;
                        player2Cards[i].transform.localPosition = newPos;
                    }
                }
                currentCard2 = 0;
                discardPhase2 =false;
            }
        }
    }
    public void GenerateCard(GameObject carta)
    {

        GameObject newgo = Instantiate(carta, Vector3.zero, Quaternion.identity, canvas.transform);
        newgo.name = carta.name;
        newgo.transform.localPosition = new Vector3(0, -400, 0);
        newCard1 = newgo;

        StartCoroutine(WaitForCardAnimation());
    }
    public void GenerateCard2(GameObject carta)
    {
        GameObject newgo = Instantiate(carta, Vector3.zero, Quaternion.identity, canvas.transform);
        newgo.name = carta.name;
        newgo.transform.localPosition = new Vector3(0, -400, 0);
        newCard2 = newgo;

        StartCoroutine(WaitForCardAnimation2());
    }
    public void CarttaDescarte1()
    {
        int ran = Random.Range(0, player1Cards.Count);
        Destroy(player1Cards[ran], 0.1f);
        player1Cards.Remove(player1Cards[ran]);
        for (int i = 0; i < player1Cards.Count; i++)
        {
            if (i == 0)
            {
                player1Cards[i].transform.localPosition = Vector3.Lerp(player1Cards[i].transform.localPosition, CardParent1.transform.localPosition, 100);
            }
            if (i != 0)
            {
                Vector3 newPos = player1Cards[i - 1].transform.localPosition;
                newPos.x += 75;
                player1Cards[i].transform.localPosition = newPos;
            }
        }
        currentCard1 = 0;
    }
    public void CarttaDescarte2()
    {
        int ran = Random.Range(0, player1Cards.Count);
        Destroy(player2Cards[ran], 0.1f);
        player2Cards.Remove(player2Cards[ran]);
        for (int i = 0; i < player2Cards.Count; i++)
        {
            if (i == 0)
            {
                player2Cards[i].transform.localPosition = Vector3.Lerp(player2Cards[i].transform.localPosition, CardParent2.transform.localPosition, 100);
            }
            if (i != 0)
            {
                Vector3 newPos = player2Cards[i - 1].transform.localPosition;
                newPos.x += 75;
                player2Cards[i].transform.localPosition = newPos;
            }
        }
        currentCard2 = 0;
    }
}
