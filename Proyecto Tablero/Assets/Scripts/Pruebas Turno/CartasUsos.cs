using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class CartasUsos : MonoBehaviour
{

    enum cardTipos{  RETROCESO, BONUSTIRADA, DESCARTE, PASAREFECTO, BLOQUEAR, INTERCAMBIODADO, RESULTADOENTREDOS  };
    // Start is called before the first frame update
    public int num;

    DiceNum diceNum;

    public GameObject player1, player2;
    private CardCanvasManager carttaManger;

    bool divisionBool;
    cardTipos cardTipos1;
    void Start()
    {

        player1 = GameObject.Find("Player1");
        player2 = GameObject.Find("Player2");
        carttaManger = GameObject.Find("CardManager").GetComponent<CardCanvasManager>();

        if (num==0){
            cardTipos1 = cardTipos.RETROCESO;
        }else if(num==1){
            cardTipos1 = cardTipos.BONUSTIRADA;
        }else if(num==2){
            cardTipos1 = cardTipos.DESCARTE;
        }else if(num==3){
            cardTipos1 = cardTipos.PASAREFECTO;
        }else if(num==4){
            cardTipos1 = cardTipos.BLOQUEAR;
        }else if(num==5){
            cardTipos1 = cardTipos.INTERCAMBIODADO;
        }else if(num==6){
            cardTipos1 = cardTipos.RESULTADOENTREDOS;
        }
    }

    public void UsarCard(int player){

        switch(cardTipos1){

            case cardTipos.RETROCESO:

                if (player == 2)
                {
                    player2.transform.GetChild(2).GetComponent<DiceNum>().backDiceEvent = true;

                }
                else
                {
                    player1.transform.GetChild(2).GetComponent<DiceNum>().backDiceEvent = true;
                }

            break;

            case cardTipos.BONUSTIRADA:
                
                if (player == 2)
                {
                    player2.transform.GetChild(2).GetComponent<DiceNum>().bonusCasilla = true;

                }
                else
                {
                    player1.transform.GetChild(2).GetComponent<DiceNum>().bonusCasilla = true;
                }
            
            break;

            case cardTipos.DESCARTE:
                if (player == 1)
                {
                    carttaManger.CarttaDescarte1();
                }
                else
                {                 
                    carttaManger.CarttaDescarte2();
                }
            break;

            case cardTipos.PASAREFECTO:

            break;

            case cardTipos.BLOQUEAR:

            break;

            case cardTipos.INTERCAMBIODADO:
                GameManager.Instance.evenetDices = true;
                break;

            case cardTipos.RESULTADOENTREDOS:
                
                if(player == 2)
                {
                    player2.transform.GetChild(2).GetComponent<DiceNum>().ActivateDivision(true);
                }
                else
                {
                     player1.transform.GetChild(2).GetComponent<DiceNum>().ActivateDivision(true);
                }
            
            break;

        }

    }
}
