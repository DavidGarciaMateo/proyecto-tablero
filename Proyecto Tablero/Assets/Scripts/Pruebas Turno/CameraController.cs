using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    private Transform Player1, Player2;
    private Vector3 camPos;
    public Animator Transition;

    private bool animationDone;

    public GameObject splitCam1, splitCam2, globalCam;
    // Start is called before the first frame update
    void Start()
    {
        Player1 = GameObject.Find("Player1").transform;
        Player2 = GameObject.Find("Player2").transform;
        splitCam1.SetActive(true);
        splitCam2.SetActive(true);
        animationDone = false;
    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(Player2.position + (Player1.position - Player2.position) / 2);
        float dist = Vector3.Distance(Player1.position, Player2.position);
        camPos = Player2.position + (Player1.position - Player2.position) / 2;
        camPos.y = dist;
        if (camPos.y >= 550)
        {
            camPos.y = 550;
        }
        if (camPos.y <= 18)
        {
            camPos.y = 18;
        }
        if (dist <= 30)
        {
            if (!animationDone)
            {
                StartCoroutine(WaitForTransition());
            }
        }
        else
        {
            if (animationDone)
            {
                StartCoroutine(WaitForTransition2());
            }
        }
        transform.position = camPos;
    }
    IEnumerator WaitForTransition()
    {
        Transition.SetBool("Close", true);
        animationDone = true;
        yield return new WaitForSeconds(0.5f);
        splitCam1.SetActive(false);
        splitCam2.SetActive(false);
        yield return new WaitForSeconds(0.5f);
        Transition.SetBool("Close", false);
    }
    IEnumerator WaitForTransition2()
    {
        Transition.SetBool("Close", true);
        animationDone = false;
        yield return new WaitForSeconds(0.5f);
        splitCam1.SetActive(true);
        splitCam2.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        Transition.SetBool("Close", false);
    }
}
