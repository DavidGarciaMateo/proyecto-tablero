using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CasillaDetection : MonoBehaviour
{
    [SerializeField] public Transform player1;
    [SerializeField] public Transform player2;
    [SerializeField] private CasillaDetection casilla;
    //Declarar el Array de las probabilidades de sacar una carta/ minijuego y su capacidad
    private float[] probMinigames = new float[5];
    public EventTeleport teleport;
    private float[] probCartas = new float[10];
    private float[] probEvents = new float[8];
    private GameObject target;
    private Transform targetTrans;
    public bool used = false, dentro = false;
    private int valor;

    public GameManager gameManager;
    public EventosManager eventosManager;
    public GameObject cartaRandom;
    public CardCanvasManager cardCanvasManager;

    [Header ("Prefabs Cartas")]

    [SerializeField] public GameObject [] cartaPref = new GameObject[7];
    // Start is called before the first frame update
    private void Awake()
    {
        //Recorre el array con un for y le asignar los valores en cada posici?n
        for (int i = 0; i < probMinigames.Length; i++)
        {
            probMinigames[i] = 20f;
        }
        for (int i = 0; i < probCartas.Length; i++)
        {
            probCartas[i] = 10f;
        }
        for (int i = 0; i < probEvents.Length; i++)
        {
            probEvents[i] = 12.5f;
        }
    }
    void Start()
    {
        target = GameObject.Find("target");
        targetTrans = GetComponent<Transform>();
        cardCanvasManager = GameObject.Find("Canvas").transform.GetChild(10).GetComponent<CardCanvasManager>();
    }
    private void Update()
    {

      
    }


    //Por si en vez de detectar las casillas con triggers se quiere hacer con un raycast
    private void ChooseEvent()
    {
        float probEvent = ProbabilitySystem(probEvents);
        switch (valor)
        {
            case 0:

                //Retroceso();

                break;
            case 1:

                //Avanza x casillas
                break;
            case 2:

                //Impar / Par
                break;
            case 3:

                //intercambia posiciones
                break;
            case 4:
                eventosManager.CambiarCartas();
                //cambiar mazos
                break;
            case 5:

                //perdida de cartas
                break;
            case 6:

                //perdida de turno
                break;
            case 7:

                //Castigo en su siguiente turno
                break;
            default:
                ChooseEvent();
                break;
        }

    }

    public void ChooseMinigame()
    {
        //Llama al calculo de probabilidad
        float probMin = ProbabilitySystem(probMinigames);
        //Reasignacion de las probabilidades despu?s de aplicar el 10%
        for (int i = 0; i < probMinigames.Length; i++)
        {
            probMinigames[i] = 20f;
        }
        //Escoge el minijuego y le disminuye la probabilidad
        switch (probMin)
        {
            case 0:
                probMinigames[0] = 10f;
                SceneManager.LoadScene("Coco");
                break;
            case 1:
                probMinigames[1] = 10f;
                SceneManager.LoadScene("Limpieza");
                 Debug.Log("Limpieza");
                break;
            case 2:
                probMinigames[2] = 10f;
                SceneManager.LoadScene("FG");
                break;
            case 3:
                probMinigames[3] = 10f;
                SceneManager.LoadScene("BF");
                 Debug.Log("Entrando a PELEA A MUERTE");
                break;
            case 4:
                probMinigames[4] = 10f;
                SceneManager.LoadScene("PR");
                 Debug.Log("Entrando a CARRERA DE FLOTADORES");
                break;
            default:
                ChooseMinigame();
                break;
        }
        used = true;
    }
    private void ChooseCard()
    {
        Debug.Log("Holaaa");    
        
         float probCard = ProbabilitySystem(probCartas);
       
        //Funcionamiento igual con las cartas pero no la probabilidad no disminuye nunca
       
        switch (probCard)
        {
            case 0:
                
                cartaRandom = cartaPref[0];
                //retroceso
                break;
            case 1:

                cartaRandom = cartaPref[1];
                //Bonus casilla
                break;
            case 2:

            cartaRandom = cartaPref[2];
                //Descartar al azar
                break;
            case 3:
            cartaRandom = cartaPref[3];
                //Pasar efecto negativo
                break;
            case 4:

            cartaRandom = cartaPref[4];
                //Bloquear Evento
                break;
            case 5:

            cartaRandom = cartaPref[5];
                //Resultado /2
                break;
            case 6:

            cartaRandom = cartaPref[6];
                //intercambio de dados
                break;
            default:
                ChooseCard();
                break;
        }
        

        used = true;

        if(transform.name == "Player1" && dentro == false){
            //Debug.Log("CAGASTE");
             cardCanvasManager.GenerateCard(cartaRandom);
             dentro = true;
             return;
        }else if(transform.name == "Player2" && dentro == false){
            //Debug.Log("CAGASTE");
             cardCanvasManager.GenerateCard2(cartaRandom);
             dentro = true;
            return;

        }

        
    }

    float ProbabilitySystem(float[] probs)
    {

        float total = 0;

        foreach (float elem in probs)
        {
            total += elem;
        }

        float randomPoint = Random.value * total;

        for (int i = 0; i < probs.Length; i++)
        {
            if (randomPoint < probs[i])
            {
                return i;
            }
            else
            {
                randomPoint -= probs[i];
            }
        }
        return probs.Length - 1;
    }
    private void OnTriggerStay(Collider other)
    {
        if (!used && player1.gameObject.GetComponent<MovePlayer>().isOnRoute == false && player2.gameObject.GetComponent<MovePlayer>().isOnRoute == false)
        {
            if (other.gameObject.CompareTag("Minijuego"))
            {

                //ChooseMinigame();
            }
            else if (other.gameObject.CompareTag("Carta"))
            {
                used = true;
                ChooseCard();
                
                
            }
            else if (other.gameObject.CompareTag("Evento A")) //Como que la generaci?n de eventos no es aleatoria es forzada, lo detecto con Tags y asignarle un valor 
                                                             //aunque tambi?n se puede hacer por nombre para no tener tanto Tag
            {

                valor = 0;
                ChooseEvent();
                used = true;
            }
            else if (other.gameObject.CompareTag("Evento B"))
            {
                valor = 1;
                ChooseEvent();
                used = true;
            }
            else if (other.gameObject.CompareTag("Evento C"))
            {

                valor = 2;
                ChooseEvent();
                used = true;
            }
            else if (other.gameObject.CompareTag("Evento D"))
            {

                valor = 3;
                ChooseEvent();
                used = true;
            }
            else if (other.gameObject.CompareTag("Evento E"))
            {

                valor = 4;
                ChooseEvent();
                used = true;
            }
            else if (other.gameObject.CompareTag("Evento F"))
            {

                valor = 5;
                ChooseEvent();
                used = true;
            }
            else if (other.gameObject.CompareTag("Evento G"))
            {

                valor = 6;
                ChooseEvent();
                used = true;
            }
            else if (other.gameObject.CompareTag("Evento H"))
            {

                valor = 7;
                ChooseEvent();
                used = true;
            }

        }

    }

    

}
