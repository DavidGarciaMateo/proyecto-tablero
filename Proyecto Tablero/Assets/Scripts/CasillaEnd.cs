using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class CasillaEnd : MonoBehaviour
{
[SerializeField] private GameObject panelEnd;
[SerializeField] private GameObject winP1;
[SerializeField] private GameObject winP2;
[SerializeField] private bool pl1Enter;
[SerializeField] private bool pl2Enter;

private GameObject player;
private GameObject winner;


private void OnTriggerStay(Collider other) {

    if(other.gameObject.name == "Player1")
    {
        pl1Enter = true;
    }
        if(other.gameObject.name == "Player2")
    {
        pl2Enter = true;
    }
    player = other.gameObject;

    if(GameManager.Instance.movePlayer1.isOnRoute==false && GameManager.Instance.movePlayer2.isOnRoute == false && pl1Enter &&pl2Enter)
    {
        StartCoroutine(WaitMiniGame());
    }
    else if(GameManager.Instance.movePlayer1.isOnRoute==false && GameManager.Instance.movePlayer2.isOnRoute == false &&(pl1Enter || pl2Enter))
    {
        winner = other.gameObject;
        panelEnd.SetActive(true);
        if( winner.name == "Player1")
        {
            winP1.SetActive(true);
            winP2.SetActive(false);
        }
        else if( winner.name == "Player2")
        {
            winP1.SetActive(false);
            winP2.SetActive(true);
        }
    }


}
    IEnumerator WaitMiniGame()
  {
      yield return new WaitForSeconds(2);
      player.GetComponent<CasillaDetection>().ChooseMinigame();
  
  }
  public void Credits()
{
    SceneManager.LoadScene("Creditos");
}

}
