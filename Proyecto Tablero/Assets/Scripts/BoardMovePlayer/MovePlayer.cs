using System.Collections;
using System.Collections.Generic;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine;
using UnityEngine.AI;

public class MovePlayer : MonoBehaviour
{
    private List<Transform> pos = new List<Transform>();
    private NavMeshAgent agent;
    public GameObject parent;
    private CasillaDetection scriptCasilla;
    [SerializeField] private int playerPos;
    [SerializeField] private int lastPos;
    [HideInInspector] public bool walking, isOnRoute, isStart;
    public int playerNum;
    public GameObject pushBtn;
    public GameObject diceObject;
    [HideInInspector] public ChangeDirection changeD;

    //A�adido por Narcis Booleana para controlar el evento de teleport
    private bool teleport;

    public int PlayerPos { get => playerPos; set => playerPos = value; }
    public bool Teleport { get => teleport; set => teleport = value; }

    void Start()
    {
        scriptCasilla = GetComponent<CasillaDetection>();
        walking = false;
       // PlayerPos = 0;



        for (int i = 0; i < parent.transform.childCount; i++)
        {
            pos.Add(parent.transform.GetChild(i));      //asignacion de la ruta inicial para el player
        }
        agent = GetComponent<NavMeshAgent>();

       if(!PlayerPrefs.HasKey("StartGame")) //Narcis/ Para que solo lo haga cuando se inicia el juego de nuevo, si se quita, fallara la colocacion del les players
        {
            PlayerPrefs.SetInt("StartGame", 0);
            agent.SetDestination(pos[0].position);      //el player se mueve a la primera posicion de su ruta
        }
    }

    private void Update()
    {
        if (walking)
        {
            if (agent.velocity == Vector3.zero)         //si el jugador llego a una casilla, va a la siguiente de la ruta definida
            {
                NextPos();
                walking = false;
            }
        }
        //Si se ativa el teleport, se reasignan las rutas del player contrario / evento cambio posicion
        if (Teleport)
        {
            Teleport = false;
            pos.Clear();
            for (int i = 0; i < parent.transform.childCount; i++)
            {
                pos.Add(parent.transform.GetChild(i));      //asignacion de la ruta inicial para el player
            }
            agent.SetDestination(pos[PlayerPos].position);
        }
    }

    public void ChangeDirection(GameObject pr)      //cambiar la ruta del player
    {
        pos.Clear();

        for (int i = 0; i < pr.transform.childCount; i++)
        {
            pos.Add(pr.transform.GetChild(i));
        }
    }
    public void MoveAgent(int num)      //el player se movera la cantidad de casillas que se ingresen por num
    {
        StartCoroutine(StarMoving(num));
    }

    private IEnumerator StarMoving(int num)
    {
        lastPos = PlayerPos;
        yield return new WaitForSeconds(2f);
        PlayerPos = PlayerPos + num;
        //NextPos();
        isStart = true;
    }

    private IEnumerator SetDestinate()      //detectar si el jugador esta caminando
    {
        yield return new WaitForSeconds(0.2f);
        walking = true;
    }

    public void NextPos()
    {
        if (lastPos < PlayerPos)
        {
            if (changeD != null)
            {
                walking = false;
                changeD.electionGO.SetActive(true);
                changeD.isActive = true;
                changeD = null;
            }
            else
            {
                lastPos++;
                transform.LookAt(pos[lastPos].position);
                agent.SetDestination(pos[lastPos].position);        //player se mueve a la siguiente casilla trazada por MoveAgent
                StartCoroutine(SetDestinate());
                print(lastPos);
            }
        }
        else if (lastPos > PlayerPos)
        {
            lastPos--;
            transform.LookAt(pos[lastPos].position);
            agent.SetDestination(pos[lastPos].position);        //player se mueve a la siguiente casilla trazada por MoveAgent
            StartCoroutine(SetDestinate());
            print(lastPos);
        }
        else                                                    //Se activa cuando el player llega a la ultima casilla de su ruta
        {
            string namePlayer = "TurnCountP"+playerNum;
            PlayerPrefs.SetInt(namePlayer,2);

            scriptCasilla.enabled = true;
            scriptCasilla.used = false;
            scriptCasilla.dentro = false;
            walking = false;
            isOnRoute = false;
            //Eliminar el llamado a esta funcion con el nuevo sistema de turnos
        }

    }

    public void NewDiceTurn()                                  //Activa la mecanica del dado
    {
        pushBtn.SetActive(true);
        diceObject.SetActive(true);
    }
    public void SimpleMove(int num)
    {
        lastPos = PlayerPos;
        PlayerPos = PlayerPos + num;
        NextPos();
    }
}
