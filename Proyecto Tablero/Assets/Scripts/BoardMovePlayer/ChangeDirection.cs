using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Animations;

public class ChangeDirection : MonoBehaviour
{
    public GameObject wayL, wayR, electionGO;
    public bool isActive;
    public int playerNum;
    private MovePlayer player1, player2;

    private void Start()
    {
        isActive = false;
        player1 = GameObject.Find("Player1").GetComponent<MovePlayer>();
        player2 = GameObject.Find("Player2").GetComponent<MovePlayer>();
    }

    private void Update()
    {
        if (isActive)
        {
            if (playerNum == 1)
            {
                if (Input.GetAxis("Xaxis") == -1)        //cambiar direccion para el player1
                {
                    player1.ChangeDirection(wayL);
                    player1.parent = wayL;
                    PlayerPrefs.SetString("RutaPL1", player1.parent.name);
                    player1.walking = true;
                    isActive = false;
                    electionGO.SetActive(false);
                    Destroy(transform.gameObject);
                }
                else if (Input.GetAxis("Xaxis") == 1)
                {

                    player1.ChangeDirection(wayR);
                    player1.walking = true;
                    player1.parent = wayR;
                    PlayerPrefs.SetString("RutaPL1", player1.parent.name);
                    isActive = false;
                    electionGO.SetActive(false);
                    Destroy(transform.gameObject);
                }
            }
            else if (playerNum == 2)
            {
                if (Input.GetKeyDown(KeyCode.A))        //cambiar direccion para el player2
                {
                    player2.ChangeDirection(wayL);
                    player2.walking = true;
                    player2.parent = wayL;
                    PlayerPrefs.SetString("RutaPL2", player2.parent.name);
                    isActive = false;
                    electionGO.SetActive(false);
                    Destroy(transform.gameObject);
                }
                else if (Input.GetKeyDown(KeyCode.D))
                {
                    player2.ChangeDirection(wayR);
                    player2.walking = true;
                    player2.parent = wayR;
                    PlayerPrefs.SetString("RutaPL2", player2.parent.name);
                    isActive = false;
                    electionGO.SetActive(false);
                    Destroy(transform.gameObject);
                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))         //si el player colsisiona, se para en la siguiente posicion y tiene que elegir su siguiente dirrecion
        {
            if (playerNum == 1)
            {
                player1.changeD = GetComponent<ChangeDirection>();
                electionGO = player1.transform.GetChild(player1.transform.childCount - 1).gameObject;
            }
            else if (playerNum == 2)
            {
                player2.changeD = GetComponent<ChangeDirection>();
                electionGO = player2.transform.GetChild(player2.transform.childCount - 1).gameObject;
            }
        }
    }
}
