using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Dice : MonoBehaviour
{
    public DiceNum master;
    private SphereCollider sph;
    private TextMeshPro numTxt;
    private bool isActive = false;

    void Start()
    {
        sph = GetComponent<SphereCollider>();
        numTxt = transform.GetChild(0).GetComponent<TextMeshPro>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Finish"))
        {
            if (!isActive)
            {
                master.SetNum(transform.GetComponent<Dice>());
                isActive = true;
            }
        }
    }

    public void ChangeActive(bool b)        //detectar el numero que del dado
    {
        sph.enabled = b;
        if (b) isActive = false;
    }

    public void ViewNum(int num)
    {
        StartCoroutine(ShowNum(num));
    }

    private IEnumerator ShowNum(int num)
    {
        numTxt.text = num.ToString();
        yield return new WaitForSeconds(1.9f);
        numTxt.text = "";
    }
}
