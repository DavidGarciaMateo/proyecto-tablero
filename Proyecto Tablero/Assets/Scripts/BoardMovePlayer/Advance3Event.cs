using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Advance3Event : MonoBehaviour
{
    //public TextMeshPro advanceTxt;
    private MovePlayer player;
    private bool activeEvent, activeDefault;
    private bool isActive = false;
    private Turnos turnM;
    private GameManager gameM;
    public bool isNegative = false;

    private void Start()
    {
        turnM = GameObject.Find("GameManager").GetComponent<Turnos>();
        gameM = turnM.gameObject.GetComponent<GameManager>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (activeEvent) activeDefault = true;
    }

    private void Update()
    {
        if (isActive)
        {
            if (!gameM.movePlayer1.isOnRoute && !gameM.movePlayer2.isOnRoute)
            {
                isActive = false;
                int playerNum = player.GetComponent<MovePlayer>().playerNum;
                if (playerNum == 1)
                {
                    turnM.finishP1 = true;
                }
                else
                {
                    turnM.finishP2 = true;
                }
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (!activeEvent)
        {
            if (other.CompareTag("Player"))
            {
                if (!gameM.movePlayer1.isOnRoute && !gameM.movePlayer2.isOnRoute)
                {
                    player = other.GetComponent<MovePlayer>();
                    StartCoroutine(AdvancePlayer());
                    activeEvent = true;
                }
            }
        }

        if (activeDefault && !gameM.movePlayer1.isOnRoute && !gameM.movePlayer2.isOnRoute && activeEvent)
        {
            int player = other.GetComponent<MovePlayer>().playerNum;
            if (player == 1)
            {
                turnM.finishP1 = true;
            }
            else
            {
                turnM.finishP2 = true;
            }
            activeDefault = false;
        }
    }

    private IEnumerator AdvancePlayer()
    {
       // advanceTxt.text = "+3";
        yield return new WaitForSeconds(1f);
        if (isNegative)
        {
            player.SimpleMove(-3);
        }
        else player.SimpleMove(3);

        player.isOnRoute = true;
        yield return new WaitForSeconds(1f);
       // advanceTxt.text = "";
        isActive = true;
    }
}
