using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DiceNum : MonoBehaviour
{
   public int diceSides;
    public Text numTxt;
    private Rigidbody rb;
    public List<Dice> dice;
    public MovePlayer player;
    private bool rotating = false;
    private Vector3 newRotation;
    public Transform firstPos;
    public bool backDiceEvent = false;

    public bool bonusCasilla;


    public bool division;

    public bool  startEventH, startEventC;
    public EventoC eventC;
    public EventoH eventH;
    private void Start()
    {
        rb = GetComponent<Rigidbody>();

        foreach (Dice d in dice)
        {
            d.master = GetComponent<DiceNum>();
        }

        int diceValue = Random.Range(1, 3);
    }

    private void OnEnable()
    {
        transform.position = firstPos.position;
        transform.rotation = firstPos.rotation;
    }

    private void Update()
    {
        if (rotating)
        {
            transform.Rotate(newRotation * Time.deltaTime * 3);     //rotar el dado en el aire
        }
    }

    public void ActivateDivision(bool b)
    {
        division = b;
    }

    public void SetNum(Dice obj)       
    {

        int diceValue = GenerateRnd(diceSides);

        if (startEventC)
        {
            startEventC = false;
            diceValue = EjecutarEventoC(diceValue);
        }
        if (startEventH)
        {
            startEventH = false;
            diceValue = EjecutarEventoH(diceValue);
        }
        if (bonusCasilla)
        {
            bonusCasilla = false;
            diceValue = diceValue + 3;
        }

        obj.ViewNum(diceValue);

        if (backDiceEvent)
        {
            diceValue *= -1;
            backDiceEvent = false;

            if(division)
            {
                diceValue = (int) (diceValue/2);

                if(diceValue==0) diceValue = -1;
                division = false;
            }

        }


        if(division){
            diceValue = (int) (diceValue/2);

            if(diceValue==0) diceValue = 1;
            division = false;

        }

        numTxt.text = diceValue.ToString();     //muestra el numero del dado por texto
        StartCoroutine(EnableDice(false, 0f));      //evita que el dado detecte mas numeros
        if(diceValue==0)
        {
            diceValue =1;
        }
        player.MoveAgent(diceValue);        //mueve al player dependiendo del numero del dado
        player.isOnRoute = true;
    }

    private int GenerateRnd(int MaxValue)
    {
        int Value = Random.Range(1, (MaxValue + 1));       //genera un valor random del numero que salio en el dado
        return Value;
    }

    public void LanzarDado()
    {
        float initialForce = Random.Range(9, 13);
        player.pushBtn.SetActive(false);
        rb.isKinematic = false;
        rb.AddForce(new Vector3(0, initialForce * 100, 0));
        RotateDice();
        StartCoroutine(EnableDice(true, 5.5f));       //detecta que numero dio el dado tras 4s
    }

    IEnumerator EnableDice(bool b, float time)      //activar o desactivar numeros del dado
    {
        yield return new WaitForSeconds(time);
        foreach (Dice d in dice)
        {
            d.ChangeActive(b);
        }
        yield return new WaitForSeconds(2f);
        if (!b)
        {
            transform.gameObject.SetActive(false);      //desactiva el dado y el texto que muestra el numero
            numTxt.text = "";
        }
    }

    private void RotateDice()       //definir angulo de rotacion random
    {
        float rotX = Random.Range(1, 90);
        float rotY = Random.Range(1, 90);
        float rotZ = Random.Range(1, 90);
        newRotation = new Vector3(rotX, rotY, rotZ);
        rotating = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Finish"))     //desactivar rotacion del dado
        {
            rotating = false;
        }
    }

    public int EjecutarEventoC(int value)
    {
        if ((value % 2) == 0)
        {
            value = 5;
        }
        else
        {
            value = -5;
        }

        return value;
    }

    public int EjecutarEventoH(int value)
    {
        if ((value % 2) == 0)
        {
            value = value - 1;
            value = value + 1;
        }
        else
        {
            value = 0;
        }

        return value;
    }
}
