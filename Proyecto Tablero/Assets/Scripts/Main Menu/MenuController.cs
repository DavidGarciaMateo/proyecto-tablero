using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Animations;

public class MenuController : MonoBehaviour
{

    [SerializeField] private GameObject paneMain, btnPlay, btnSettings, btnExit, paneSettings, paneVolume, paneCredits, paneControls, btnsMain;

    [SerializeField] private Animator paneSettingsAnim;

    public bool desplegado;
    // Start is called before the first frame update
private void Awake() {
        PlayerPrefs.DeleteAll();
    
}
    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlayBtn(){
        SceneManager.LoadScene("Tablero");
    }

    public void SettingsBtn(){
        paneMain.SetActive(false);
        paneSettings.SetActive(true);

        paneSettingsAnim.SetBool("Desplegado", true);

        
    }

    public void SettingsBackBtn(){

        paneSettingsAnim.SetBool("Desplegado", false);
        
        paneMain.SetActive(true);
        //paneSettings.SetActive(false);
    }

    public void VolumeBtn(){
        //paneSettings.SetActive(false);
        paneVolume.SetActive(true);
    }

    public void VolumeBackBtn(){
        //paneSettings.SetActive(true);
        paneVolume.SetActive(false);
    }

    public void ControlsBtn(){
        Debug.Log("Entrando en panel controles");
    }

    public void CreditsBtn(){
        Debug.Log("Entrando en panel creditos");
    }

    public void ExitBtn(){
        Application.Quit();
    }


}
