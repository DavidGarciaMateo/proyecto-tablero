Shader "Subatella/Water"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}

        //Ceramos  variable de opacidad
        _Opacity ("Opacity",Range(0,1)) = 0.5

        //Animaciones para mover las UVs y generar moviiento
        _AnimSpeedX ("Anim Speed X",Range(0,4)) = 1.3
        _AnimSpeedY ("Anim Speed Y",Range(0,4)) = 2.7

        //HAcemos tillings para q no se estire la textura
        _AnimTilling("Anim Talling",Range(0,20)) = 8

        //Ajustar escala  para controlar la deformación
        _AnimScale("Anim Scale", Range(0,1)) = 0.3

    }
    SubShader
    {
        //Debemos poner transparente
        Tags { "RenderType"="Transparent" "Queue" = "Transparent" }
        LOD 100
        ZWrite Off
        //Apagamos la profundidad de la textura
        Blend SrcAlpha OneMinusSrcAlpha
        //Le decimos que coja el alpha


        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            half _Opacity;
            // half es como un float pero con menos capacidad

            float _AnimSpeedX;
            float _AnimSpeedY;
            float _AnimTilling;
            float _AnimScale;



            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                //mover UVs
                //Time tiene varios tipos, .x, .y y .z. //  .y equivale a deltatime
               // i.uv.x += _Time.y * _AnimSpeedX;
               // i.uv.y += _Time.y * _AnimSpeedY;

                // Formula para producir olas
                i.uv.x += sin((i.uv.x + i.uv.y)* _AnimTilling +_Time.y*_AnimSpeedX) *_AnimScale;
                i.uv.y += cos((i.uv.x + i.uv.y)* _AnimTilling +_Time.y*_AnimSpeedY) * _AnimScale;


                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv);
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);

                //Le pasa la opacidad guardada en la varaible
                col.a = _Opacity;

                return col;
            }
            ENDCG
        }
    }
}
