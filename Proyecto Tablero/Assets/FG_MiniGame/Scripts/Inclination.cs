using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inclination : MonoBehaviour
{
    public Vector3 pos1;
    public Transform parent;
    private int ran;
    public List<Plataform> pieces;
    private bool enter;
    private bool incli;
    public float time;

    private void Start()
    {
        enter = false;
        incli = false;
        ran = Random.Range(1, 3);
        print(ran);
    }

    private void Update()
    {
        if (incli)
        {
            parent.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(pos1), Time.deltaTime * 4f);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (!enter)
            {
                enter = true;
                StartCoroutine(RanDestroy());
            }
        }
    }

    private IEnumerator Inclinate()
    {
        yield return new WaitForSeconds(time);
        incli = true;
    }

    private IEnumerator RanDestroy()
    {
        yield return new WaitForSeconds(time);
        foreach(Plataform plat in pieces)
        {
            try{
                plat.RanDestroy();
            }
            catch
            {

            }
        }
    }
}
