using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plataform : MonoBehaviour
{
    public int piso;
    public Material mat01;
    private Material mat02;
    private MeshRenderer mesh;
    private GameObject obj;
    private bool enter;
    private AudioSource countAudio;

    void Start()
    {
        enter = false;
        obj = transform.parent.gameObject;
        mat01 = obj.GetComponent<Destroy>().mat01;
        mesh = obj.GetComponent<MeshRenderer>();
        countAudio = obj.GetComponent<AudioSource>();
        mat02 = mesh.material;
    }

    IEnumerator DestroyObj1()
    {
        mesh.material = mat01;
        yield return new WaitForSeconds(1f);
        mesh.material = mat02;
        yield return new WaitForSeconds(1f);
        mesh.material = mat01;
        yield return new WaitForSeconds(1f);
        mesh.material = mat02;
        yield return new WaitForSeconds(1f);
        mesh.material = mat01;
        yield return new WaitForSeconds(1f);
        Destroy(obj);
    }

    IEnumerator DestroyObj2()
    {
        mesh.material = mat01;
        yield return new WaitForSeconds(0.8f);
        mesh.material = mat02;
        yield return new WaitForSeconds(0.8f);
        mesh.material = mat01;
        yield return new WaitForSeconds(0.8f);
        mesh.material = mat02;
        yield return new WaitForSeconds(0.8f);
        mesh.material = mat01;
        yield return new WaitForSeconds(0.8f);
        Destroy(obj);
    }

    IEnumerator DestroyObj3()
    {
        mesh.material = mat01;
        yield return new WaitForSeconds(0.6f);
        mesh.material = mat02;
        yield return new WaitForSeconds(0.6f);
        mesh.material = mat01;
        yield return new WaitForSeconds(0.6f);
        mesh.material = mat02;
        yield return new WaitForSeconds(0.6f);
        mesh.material = mat01;
        yield return new WaitForSeconds(0.6f);
        Destroy(obj);
    }

    IEnumerator DestroyObj4()
    {
        mesh.material = mat01;
        yield return new WaitForSeconds(0.4f);
        mesh.material = mat02;
        yield return new WaitForSeconds(0.4f);
        mesh.material = mat01;
        yield return new WaitForSeconds(0.4f);
        mesh.material = mat02;
        yield return new WaitForSeconds(0.4f);
        mesh.material = mat01;
        yield return new WaitForSeconds(0.4f);
        Destroy(obj);
    }

    public void RanDestroy()
    {
        enter = true;
        StartCoroutine(DestroyObj4());
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (!enter)
            {
                countAudio.Play();
                enter = true;
                if (piso == 1)
                {
                    StartCoroutine(DestroyObj1());
                }
                else if (piso == 2)
                {
                    StartCoroutine(DestroyObj2());
                }
                else if (piso == 3)
                {
                    StartCoroutine(DestroyObj3());
                }
                else if (piso == 4)
                {
                    StartCoroutine(DestroyObj4());
                }
            }
        }
    }
}
