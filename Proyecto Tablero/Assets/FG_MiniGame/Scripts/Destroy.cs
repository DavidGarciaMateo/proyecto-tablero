using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroy : MonoBehaviour
{
    private GameObject collide;
    public Material mat01;
    public int piso;
    void Start()
    {
        collide = transform.GetChild(0).gameObject;
        collide.GetComponent<Plataform>().piso = piso;
        StartCoroutine(StartGame());
    }

    IEnumerator StartGame()
    {
        yield return new WaitForSeconds(6.5f);
        collide.GetComponent<Plataform>().enabled = true;
        //collide.GetComponent<Plataform>().piso = piso;
        collide.GetComponent<MeshCollider>().enabled = true;
    }
}
