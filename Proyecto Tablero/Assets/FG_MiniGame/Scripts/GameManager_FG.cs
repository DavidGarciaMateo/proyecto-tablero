using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;
using UnityEngine.SceneManagement;

public class GameManager_FG : MonoBehaviour
{
    public Text playerTxt, actionTxt;
    public GameObject finishPanel;
    public GameObject player1, player2;
    public Transform pos1, pos2;
    public GameObject camera1, camera2, cameraF;
    public GameObject text1, text2;
    public GameObject initialCamara, cam1, cam2, cam3;

    private void Start()
    {
        StartCoroutine(InitialCinematic());
    }

    public void ResetLevel()
    {
        SceneManager.LoadScene("Tablero");
    }

    IEnumerator InitialCinematic()
    {
        actionTxt.text = "3";
        yield return new WaitForSeconds(1.5f);
        actionTxt.text = "2";
        yield return new WaitForSeconds(1.5f);
        actionTxt.text = "1";
        yield return new WaitForSeconds(1.5f);
        actionTxt.text = "Start";
        yield return new WaitForSeconds(1f);
        actionTxt.text = "";
        initialCamara.SetActive(false);
        text1.SetActive(true);
        text2.SetActive(true);
        camera1.SetActive(true);
        camera2.SetActive(true);
        cam1.SetActive(false);
        cam2.SetActive(false);
        cam3.SetActive(false);

        player1.GetComponent<MovePlayer_FG>().enabled = true;
        player2.GetComponent<MovePlayer_FG>().enabled = true;
    }

    public void EndGame(int num)
    {
        finishPanel.SetActive(true);
        player1.GetComponent<CharacterController>().enabled = false;
        player2.GetComponent<CharacterController>().enabled = false;

        player1.GetComponent<MovePlayer_FG>().enabled = false;
        player2.GetComponent<MovePlayer_FG>().enabled = false;

        player1.GetComponent<Animator>().SetFloat("direction", 0);
        player2.GetComponent<Animator>().SetFloat("direction", 0);

        player2.GetComponent<Rigidbody>().isKinematic = true;
        player1.GetComponent<Rigidbody>().isKinematic = true;

        player1.GetComponent<Animator>().SetBool("jump", false);
        player2.GetComponent<Animator>().SetBool("jump", false);
        player1.GetComponent<Animator>().SetBool("push", false);
        player2.GetComponent<Animator>().SetBool("push", false);

        text1.SetActive(false);
        text2.SetActive(false);
        camera1.SetActive(false);
        camera2.SetActive(false);
        cameraF.SetActive(true);

        if (num == 1)
        {
            player1.transform.rotation = pos1.rotation;
            player1.transform.position = pos1.position;
            player2.transform.rotation = pos2.rotation;
            player2.transform.position = pos2.position;

            player1.GetComponent<Animator>().SetBool("win", true);
            player2.GetComponent<Animator>().SetBool("lose", true);

            PlayerPrefs.SetString("Preferencia", "P1");
            PlayerPrefs.SetString("1D6", "P1");
        }
        else if(num == 2)
        {
            player1.transform.rotation = pos2.rotation;
            player1.transform.position = pos2.position;
            player2.transform.rotation = pos1.rotation;
            player2.transform.position = pos1.position;

            player1.GetComponent<Animator>().SetBool("lose", true);
            player2.GetComponent<Animator>().SetBool("win", true);

            PlayerPrefs.SetString("Preferencia", "P2");
            PlayerPrefs.SetString("1D6", "P2");
        }

        string txt = "Player " + num + " Wins";
        playerTxt.text = txt;
    }
}
