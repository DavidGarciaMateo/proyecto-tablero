using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MovePlayer_FG : MonoBehaviour
{
    private Transform player;
    private CharacterController controller;
    public int playerNum;
    public KeyCode jump;
    public KeyCode fire;
    public float jumpSpeed;
    private Vector3 move = Vector3.zero;
    private GameManager_FG gameM;
    public Transform target;
    private bool detect;
    private Animator animator;
    private Vector3 movement;
    private bool jumpB, jumping, jumpCondition;
    [SerializeField] private float speed = 5;
    [SerializeField] private float speedRotation = 5;
    [SerializeField] private float gravity = -9.8f;
    private int playerNumber;

    private void Awake()
    {
        if(playerNum == 1)
        {
            playerNumber = 1;
        }
        else
        {
            playerNumber = 2;
        }

        detect = false;
        animator = GetComponent<Animator>();
        player = GetComponent<Transform>();
        controller = GetComponent<CharacterController>();
        gameM = GameObject.Find("GameManager").GetComponent<GameManager_FG>();
        jumpB = false;
        jumpCondition = false;
        animator.SetBool("jump", false);
    }
    void Update()
    {
        float vertical;
        float horizontal;

        if(playerNumber == 1)
        {
            vertical = Input.GetAxis("Vertical1");
            horizontal = Input.GetAxis("Horizontal1");
        }
        else
        {
            vertical = Input.GetAxis("Yaxis");
            horizontal = Input.GetAxis("Xaxis");
        }

        if (controller.isGrounded)
        {
            jumping = true;

            move = Vector3.forward * vertical;

            move = transform.TransformDirection(move);

            move *= speed;

            //transform.Rotate(Vector3.up * speedRotation * horizontal * Time.deltaTime);

            animator.SetFloat("direction", vertical);
        }

        transform.Rotate(Vector3.up * speedRotation * horizontal * Time.deltaTime);

        if (!jumpCondition)
        {
            if (Input.GetKeyDown(jump))
            {
                if (jumping)
                {

                    print("salta");
                    move.y += jumpSpeed;
                    jumpCondition = true;
                    if (!jumpB)
                    {
                        animator.SetBool("jump", true);
                    }

                    StartCoroutine(JumpAnim());
                }
            }
            else if (!detect)
            {
                if (Input.GetKeyDown(fire))
                {
                    detect = true;
                    animator.SetBool("push", true);
                    StartCoroutine(Fire());
                }
            }
        }

        move.y += gravity * Time.deltaTime;

        controller.Move(move * Time.deltaTime);
    }

    private IEnumerator JumpAnim()
    {
        yield return new WaitForSeconds(0.3f);
        jumping = false;
        animator.SetBool("push", false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Finish"))
        {
            gameM.EndGame(playerNum);
        }

        if (!jumping)
        {
            animator.SetBool("jump", false);
            animator.SetFloat("direction", 0);
            jumpCondition = false;
        }
    }

    private IEnumerator Fire()
    {
        if(DistancePlayerEnemy() < 7f)
        {
            target.GetComponent<Rigidbody>().isKinematic = false;
            target.GetComponent<CharacterController>().enabled = false;
            target.GetComponent<Rigidbody>().AddForce(GetPlayerEnemyDirection(target) * 1200);
            print("fire");
        }
        yield return new WaitForSeconds(0.5f);
        animator.SetBool("push", false);
        target.GetComponent<CharacterController>().enabled = true;
        target.GetComponent<Rigidbody>().isKinematic = true;
        yield return new WaitForSeconds(1f);
        detect = false;
    }

    private Vector3 GetPlayerEnemyDirection(Transform enemyTR)
    {
        return enemyTR.transform.position - transform.position;
    }

    private float DistancePlayerEnemy()
    {
        return Vector3.Distance(target.transform.position, transform.position);
    }
}
