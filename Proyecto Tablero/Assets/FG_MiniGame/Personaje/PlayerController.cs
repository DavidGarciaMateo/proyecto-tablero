using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private float graviy = -9.8f;

    [SerializeField] private float speed = 5;

    [SerializeField] private float rotationSpeed = 1.5f;

    [SerializeField] private float jumpForce = 20;

    private CharacterController controller;

    private Vector3 move = Vector3.zero;

    public Vector2 directionMove = Vector2.zero;

    private bool jump = false;

    private Animator animator;

    void Start()
    {
        controller = GetComponent<CharacterController>();

        animator = GetComponent<Animator>();
    }

    void Update()
    {
        float vertical = Input.GetAxis("Vertical");
        float horizontal = Input.GetAxis("Horizontal");
        if (controller.isGrounded)
        {
            animator.SetBool("jump", false);

            move = Vector3.forward * vertical;

            move = transform.TransformDirection(move);

            move *= speed;

            transform.Rotate(Vector3.up * rotationSpeed * Time.deltaTime * horizontal);

            if (Input.GetKeyDown(KeyCode.Space))
            {

                move.y += Mathf.Sqrt(Mathf.Abs(jumpForce * 3 * graviy));

                animator.SetBool("jump", true);
            }
        }

        move.y += graviy * Time.deltaTime;

        controller.Move(move * Time.deltaTime);

        animator.SetFloat("direction", vertical);

    }
}
