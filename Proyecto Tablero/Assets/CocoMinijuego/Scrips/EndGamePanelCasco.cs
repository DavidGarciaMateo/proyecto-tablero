using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EndGamePanelCasco : MonoBehaviour
{
    [Header("Panel a actuivar")]
    [SerializeField] private GameObject endPanel; /*Panel que se activara */

    [Header("Players")]
    [SerializeField]private Transform pj1; /*Player1*/
    [SerializeField]private Transform pj2; /*player2 */

    public Spawner[] spawner;
    [Header("Posiciones")]

    //hay que crear dos emptys en la escena, y colocarlos teneiendo en cuenta de referencia de los objetos "WinPLayerPos" y "LoosPlayerPos", que estan en la jerarquiar del panel
    [SerializeField]private Transform winPos; /*Vincular a empty object, el codigo colocara al ganador */
    [SerializeField]private Transform losePos; /*Vincular a empty object, el codigo colocara al Perdedor */

    [Header("TextoWin")]
    [SerializeField]private Text playerWinerText;

    [Header("Camara a activar")]
    //cambio de camara del guego a una propia del panel
    [SerializeField]private Camera gameCamera; 
    [SerializeField]private Camera endCamera;


    //llamar la funcion donde convenga, en mi caso, lo he modificado y lo activo al activar el panel por animación

    /*private void OnEnable() {
     MatchFinish2(GameManagerPFB.Instance.player1Win, GameManagerPFB.Instance.player2Win);
    }*/



    public void MatchFinish2(bool player1Win,bool plyer2Win)/*Vinculad una bolena para cada player, ponerla true, equivale a que ese plyer gana */
    {
        endPanel.SetActive(true);

        pj1.gameObject.GetComponent<CharacterController>().enabled = false;
        pj2.gameObject.GetComponent<CharacterController>().enabled = false;
        pj1.gameObject.GetComponent<CharController>().enabled = false;
        pj2.gameObject.GetComponent<CharController2>().enabled = false;

        for (int i = 0; i < spawner.Length; i++)
        {
            spawner[i].gaming = false;
        }

        if (player1Win)
        {
            pj1.position = winPos.position;
            pj1.rotation = winPos.rotation;
            pj2.position = losePos.position;
            pj2.rotation = losePos.rotation;

            playerWinerText.gameObject.SetActive(true);
            playerWinerText.text = "Player1 "+ "Win";
           
            //guarda quien gana para determinar quien tine el 1d6 y la preferncia de turno con las cartas
            PlayerPrefs.SetString("Preferencia","P1");
            PlayerPrefs.SetString("1D6","P1");

            //Cambio de camara
            endCamera.enabled=true;
            gameCamera.enabled = false;
        }
        else if (plyer2Win)
        {
            pj2.position = winPos.position;
            pj1.position = losePos.position;
            pj1.rotation = losePos.rotation;
            pj2.rotation = winPos.rotation;

            playerWinerText.gameObject.SetActive(true);
            playerWinerText.text = "Player2 "+ "Win";
           
            PlayerPrefs.SetString("Preferencia","P2");
            PlayerPrefs.SetString("1D6","P2");

            //fuciones propias de cada proyecto
            endCamera.enabled=true;
            gameCamera.enabled = false;

        }


    }
    public void ExitMiniGame()
    { 
        //Hay q defir la escenar
        SceneManager.LoadScene(1);
    }
}

