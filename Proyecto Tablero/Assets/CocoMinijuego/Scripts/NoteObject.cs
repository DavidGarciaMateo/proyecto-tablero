using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteObject : MonoBehaviour
{
    // Start is called before the first frame update

    private bool posibleAcierto;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D other){

        if(other.tag == "Activator" ){
            posibleAcierto = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other){

        if(other.tag == "Activator" ){
            posibleAcierto = false;
        }
    }
}
