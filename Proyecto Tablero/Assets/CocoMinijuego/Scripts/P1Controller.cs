using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;

public class P1Controller : MonoBehaviour
{
    //Variable para controlar cuando es variable una nota
    public bool valid, alreadyHit, tiebreakOn;
    public float cameraShake1, cameraShake2;
    private Image imagen, imagenCircle, tiebreakImage;
    public int aciertos = 0, aciertosTiebreak = 0, tiebreakGoal = 10;

    private string note, tiebreakWinner;

    public Sprite circleImg, circleImgGlow;
    public GameObject tiebreakObj;

   
    public Text score, tiebreakTxt;

    public AudioSource goodHit, badHit;

    public Animator animator;

    public CameraShake cameraShake;

    public Notas notas;

    public DestroyerController destroyerController;
    public GameObject bad;
    // Start is called before the first frame update
    public GameObject panelWin;
    public bool toExit;

    void Awake ()
    {
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 50;
    }

    void Start(){

        imagenCircle = this.GetComponent<Image>();
        
        //good.SetActive(false);
        bad.SetActive(false);
        
    }   


    public void OnUP(){
//EL CHIVATON
//Debug.Log("En flecha ARRIBA y tiebreak, TIEBREAKON = "+tiebreakOn+" , ALREADYHIT = "+alreadyHit+" , LA NOTA = "+note);



        if(valid == true && note == "arrowUp" && alreadyHit == false && tiebreakOn==false){
            goodHit.Play();
            aciertos++;
            //good.SetActive(true);

            StartCoroutine(cameraShake.Shake(cameraShake1, cameraShake2));
            alreadyHit = true;

            animator.SetBool("headbuttOn", true);


        }else if(tiebreakOn == true && note == "arrowUp"){
            goodHit.Play();

            Debug.Log("Estoy en el tiebreak");
            alreadyHit = false;
            aciertosTiebreak++;


        }else{
            badHit.Play();
            //Debug.Log("Cooldown");
            alreadyHit = true;
            bad.SetActive(true);
        }
 
    }

    public void OnDOWN(){


        if(valid == true && note == "arrowDown" && alreadyHit == false && tiebreakOn==false){
            goodHit.Play();
            aciertos++;
            //good.SetActive(true);

            StartCoroutine(cameraShake.Shake(cameraShake1, cameraShake2));

            alreadyHit = true;

            animator.SetBool("headbuttOn", true);

        }else if(tiebreakOn == true && note == "arrowDown"){
            goodHit.Play();

            Debug.Log("Estoy en el tiebreak");
            alreadyHit = false;
            aciertosTiebreak++;

        }else{
            badHit.Play();
            //Debug.Log("Cooldown");
            alreadyHit = true;
            bad.SetActive(true);
        }

    }

    public void OnRIGHT(){



        if(valid == true && note == "arrowRight" && alreadyHit == false && tiebreakOn==false){
            goodHit.Play();
            aciertos++;
            //good.SetActive(true);

            StartCoroutine(cameraShake.Shake(cameraShake1, cameraShake2));
            alreadyHit = true;

            animator.SetBool("headbuttOn", true);

        }else if(tiebreakOn == true &&  note == "arrowRight"){
            goodHit.Play();
           
            alreadyHit = false;
            aciertosTiebreak++;

        }else{
            badHit.Play();

            Debug.Log("Cooldown");
            alreadyHit = true;
            bad.SetActive(true);
        }

         
    }

    public void OnLEFT(){


        if(valid == true && note == "arrowLeft" && alreadyHit == false && tiebreakOn==false){
            goodHit.Play();
            aciertos++;
            //good.SetActive(true);
 
            StartCoroutine(cameraShake.Shake(cameraShake1, cameraShake2));
            alreadyHit = true;

            //animator.SetBool("winP1", true);
            animator.SetBool("headbuttOn", true);

        }else if(tiebreakOn == true && note == "arrowLeft"){
            goodHit.Play();
            
            alreadyHit = false;
            aciertosTiebreak++;

        }else{
            badHit.Play();
            //Debug.Log("Cooldown");

            alreadyHit = true;
            bad.SetActive(true);

        }

    }

     private void OnTriggerEnter2D(Collider2D other){
        
        //Debug.Log(valid);
        if(other.gameObject.tag == "Arrow"){

            valid = true;

            imagen = other.gameObject.GetComponent<Image>();
            
            note = imagen.sprite.name;

            imagenCircle.sprite = circleImgGlow;

           
           if(destroyerController.destroyNotes == 24){
                notas.beatTempo = 700;
            }else if(destroyerController.destroyNotes == 58){
                notas.beatTempo = 1100;   
            }


        }else if(other.gameObject.tag == "Tiebreak"){
            tiebreakOn = true;
            
            
            alreadyHit = false;
            
            imagen = other.gameObject.GetComponent<Image>();
            
            note = imagen.sprite.name;

            Debug.Log("TIEBREAK nota = "+note);

            

        }
        

    }

     private void OnTriggerExit2D(Collider2D other){

        //tiebreakOn = false;

        if(other.gameObject.tag == "Arrow"){

            valid = false;

            //note = "";
            
            alreadyHit = false;

            animator.SetBool("headbuttOn", false);

            imagenCircle.sprite = circleImg;

            //good.SetActive(false);
            bad.SetActive(false);

        }

    }


    void Update(){

        

        score.text = aciertos.ToString();
        tiebreakTxt.text = aciertosTiebreak.ToString();

        if(tiebreakObj.activeSelf == false){
            tiebreakOn = false;
        }

        //Debug.Log(tiebreakObj.activeSelf);

    }
public void OnExit(InputValue value)
{

if(value.isPressed)
{
   if(toExit)
   {
    SceneManager.LoadScene("Tablero");
   } 

}
}

}
