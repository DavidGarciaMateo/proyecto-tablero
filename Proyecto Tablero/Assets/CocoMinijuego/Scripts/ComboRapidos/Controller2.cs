using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Controller2 : MonoBehaviour
{

   public int coco1Lifes = 1, coco2Lifes = 1;

    public Text textScreen;

    public Sprite arrowUp, arrowDown, arrowLeft, arrowRight;
    public Image img1 ,img2, img3, img4;
    public static Controller2 instance;
    public char []code = new char[4], codeCharacters = {'w','a','s','d'};

    // Update is called once per frame
    void Start()
    {
        //supongamos que se genera de forma aleatoria


        
        GenerateCode();
        

        
    }


void Update(){

    if(coco1Lifes ==0){
        textScreen.text = "El jugador 1 ha perdido!";
    }

    if(coco2Lifes ==0){
        textScreen.text = "El jugador 2 ha perdido!";
    }

}
    void Awake(){

        if(instance ==null){
            instance =this;
        }
    }


    public void GenerateCode(){

         code = new char[4];
     
         for (int i = 0; i < code.Length; i++)
        {
            code[i]= codeCharacters[Random.Range(0,5)];

            if(i == 0){

                if(code[i] == 'w'){
                    
                    img1.sprite = arrowUp;

                }else if(code[i] == 'a'){

                    img1.sprite = arrowLeft;

                }else if(code[i] == 's'){

                    img1.sprite = arrowDown;

                }else if(code[i] == 'd'){

                    img1.sprite = arrowRight;

                }


            }else if(i == 1){

                 if(code[i] == 'w'){
                    
                    img2.sprite = arrowUp;

                }else if(code[i] == 'a'){

                    img2.sprite = arrowLeft;

                }else if(code[i] == 's'){

                    img2.sprite = arrowDown;

                }else if(code[i] == 'd'){

                    img2.sprite = arrowRight;

                }
                
            }else if(i == 2){

                 if(code[i] == 'w'){
                    
                    img3.sprite = arrowUp;

                }else if(code[i] == 'a'){

                    img3.sprite = arrowLeft;

                }else if(code[i] == 's'){

                    img3.sprite = arrowDown;

                }else if(code[i] == 'd'){

                    img3.sprite = arrowRight;

                }
                
            }else if(i == 3){

                 if(code[i] == 'w'){
                    
                    img4.sprite = arrowUp;

                }else if(code[i] == 'a'){

                    img4.sprite = arrowLeft;

                }else if(code[i] == 's'){

                    img4.sprite = arrowDown;

                }else if(code[i] == 'd'){

                    img4.sprite = arrowRight;

                }
                
            }
        }

   
        
/*

        for (int j = 0; j < code.Length; j++)
        {
            Debug.Log(" ~ "+code[j]);
        }
        code[0] = 'w';

        code[1] = 's';

        code[2] = 'a';

        code[3] = 'd';
*/

    }
}
