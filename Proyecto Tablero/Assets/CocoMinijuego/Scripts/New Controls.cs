// GENERATED AUTOMATICALLY FROM 'Assets/CocoMinijuego/Scripts/New Controls.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @NewControls : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @NewControls()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""New Controls"",
    ""maps"": [
        {
            ""name"": ""Player 1"",
            ""id"": ""8ad92b5d-01c1-49b5-b837-9d4a48533931"",
            ""actions"": [
                {
                    ""name"": ""UP"",
                    ""type"": ""Button"",
                    ""id"": ""5d350e52-f8b2-4327-b0fd-48d0cd6ea234"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""DOWN"",
                    ""type"": ""Button"",
                    ""id"": ""9d3d4ec4-62ff-4848-89b3-d29bb3cfd60d"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""RIGHT"",
                    ""type"": ""Button"",
                    ""id"": ""4d25d864-f1d4-441e-9656-959553271cd8"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""LEFT"",
                    ""type"": ""Button"",
                    ""id"": ""03bb0232-720b-4eb4-a091-009b4c728ef1"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""Exit"",
                    ""type"": ""Button"",
                    ""id"": ""018258f8-b1e6-4f0f-b3ee-df8030c07bd8"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press""
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""2907bb88-1ea3-4181-99d5-8e80d4febffb"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""UP"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""e9e4ffda-b776-4df5-9a3a-1f4bd71e05b2"",
                    ""path"": ""<Gamepad>/dpad/up"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""UP"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""8cf51596-cfe7-49b8-b72c-ef15841e59d9"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DOWN"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""67a69208-2337-496c-ac9e-95c680cb9d8f"",
                    ""path"": ""<Gamepad>/dpad/down"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DOWN"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""066fbd8d-9a2e-49a2-b468-2f0e5834ffe9"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""RIGHT"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a25aad1e-be80-4bc1-a3d4-8b455ecaa948"",
                    ""path"": ""<Gamepad>/dpad/right"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""RIGHT"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""c497312a-3971-4b60-9d53-48b990f43aa8"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""LEFT"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""5fe87737-9a94-49a0-ad2d-cb295493ba03"",
                    ""path"": ""<Gamepad>/dpad/left"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""LEFT"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""043ef0c4-51ee-498f-9615-9be811652922"",
                    ""path"": ""<Keyboard>/x"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Exit"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""04df43a9-f7df-441b-9f90-51a4b41621ee"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Exit"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""Player 2"",
            ""id"": ""d9939283-eb07-492d-911d-79555a12eade"",
            ""actions"": [
                {
                    ""name"": ""UP"",
                    ""type"": ""Button"",
                    ""id"": ""0fdb9e69-cd14-4641-8394-047ee2ab22f7"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""DOWN"",
                    ""type"": ""Button"",
                    ""id"": ""590ed73d-3cd8-43b7-97cf-e791512f47a0"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""RIGHT"",
                    ""type"": ""Button"",
                    ""id"": ""84bf7f23-5b10-46d5-a2de-c6979efa5bea"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""LEFT"",
                    ""type"": ""Button"",
                    ""id"": ""5265b6d6-19d2-46c4-9bb3-f60775ecd10b"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press""
                },
                {
                    ""name"": ""Exit"",
                    ""type"": ""Button"",
                    ""id"": ""3250bb8e-346f-4403-8d69-6712f2d7cdd1"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": ""Press""
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""fb332f51-3e54-45bf-9c69-3ca04acb40b7"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""UP"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b7254505-89bf-4e7b-834e-348ba0086176"",
                    ""path"": ""<Keyboard>/downArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""DOWN"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""f5d49fa0-30d9-406e-83df-d22a5487d869"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""RIGHT"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""8ffae540-57c8-4ba2-97c2-213964d1ea1c"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""LEFT"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""a72797f0-58e0-45ce-b820-9ba470b3dcb1"",
                    ""path"": ""<Keyboard>/x"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Exit"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b3c047f7-f37f-4d76-880e-148b75acdba1"",
                    ""path"": ""<Gamepad>/buttonSouth"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Exit"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // Player 1
        m_Player1 = asset.FindActionMap("Player 1", throwIfNotFound: true);
        m_Player1_UP = m_Player1.FindAction("UP", throwIfNotFound: true);
        m_Player1_DOWN = m_Player1.FindAction("DOWN", throwIfNotFound: true);
        m_Player1_RIGHT = m_Player1.FindAction("RIGHT", throwIfNotFound: true);
        m_Player1_LEFT = m_Player1.FindAction("LEFT", throwIfNotFound: true);
        m_Player1_Exit = m_Player1.FindAction("Exit", throwIfNotFound: true);
        // Player 2
        m_Player2 = asset.FindActionMap("Player 2", throwIfNotFound: true);
        m_Player2_UP = m_Player2.FindAction("UP", throwIfNotFound: true);
        m_Player2_DOWN = m_Player2.FindAction("DOWN", throwIfNotFound: true);
        m_Player2_RIGHT = m_Player2.FindAction("RIGHT", throwIfNotFound: true);
        m_Player2_LEFT = m_Player2.FindAction("LEFT", throwIfNotFound: true);
        m_Player2_Exit = m_Player2.FindAction("Exit", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Player 1
    private readonly InputActionMap m_Player1;
    private IPlayer1Actions m_Player1ActionsCallbackInterface;
    private readonly InputAction m_Player1_UP;
    private readonly InputAction m_Player1_DOWN;
    private readonly InputAction m_Player1_RIGHT;
    private readonly InputAction m_Player1_LEFT;
    private readonly InputAction m_Player1_Exit;
    public struct Player1Actions
    {
        private @NewControls m_Wrapper;
        public Player1Actions(@NewControls wrapper) { m_Wrapper = wrapper; }
        public InputAction @UP => m_Wrapper.m_Player1_UP;
        public InputAction @DOWN => m_Wrapper.m_Player1_DOWN;
        public InputAction @RIGHT => m_Wrapper.m_Player1_RIGHT;
        public InputAction @LEFT => m_Wrapper.m_Player1_LEFT;
        public InputAction @Exit => m_Wrapper.m_Player1_Exit;
        public InputActionMap Get() { return m_Wrapper.m_Player1; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(Player1Actions set) { return set.Get(); }
        public void SetCallbacks(IPlayer1Actions instance)
        {
            if (m_Wrapper.m_Player1ActionsCallbackInterface != null)
            {
                @UP.started -= m_Wrapper.m_Player1ActionsCallbackInterface.OnUP;
                @UP.performed -= m_Wrapper.m_Player1ActionsCallbackInterface.OnUP;
                @UP.canceled -= m_Wrapper.m_Player1ActionsCallbackInterface.OnUP;
                @DOWN.started -= m_Wrapper.m_Player1ActionsCallbackInterface.OnDOWN;
                @DOWN.performed -= m_Wrapper.m_Player1ActionsCallbackInterface.OnDOWN;
                @DOWN.canceled -= m_Wrapper.m_Player1ActionsCallbackInterface.OnDOWN;
                @RIGHT.started -= m_Wrapper.m_Player1ActionsCallbackInterface.OnRIGHT;
                @RIGHT.performed -= m_Wrapper.m_Player1ActionsCallbackInterface.OnRIGHT;
                @RIGHT.canceled -= m_Wrapper.m_Player1ActionsCallbackInterface.OnRIGHT;
                @LEFT.started -= m_Wrapper.m_Player1ActionsCallbackInterface.OnLEFT;
                @LEFT.performed -= m_Wrapper.m_Player1ActionsCallbackInterface.OnLEFT;
                @LEFT.canceled -= m_Wrapper.m_Player1ActionsCallbackInterface.OnLEFT;
                @Exit.started -= m_Wrapper.m_Player1ActionsCallbackInterface.OnExit;
                @Exit.performed -= m_Wrapper.m_Player1ActionsCallbackInterface.OnExit;
                @Exit.canceled -= m_Wrapper.m_Player1ActionsCallbackInterface.OnExit;
            }
            m_Wrapper.m_Player1ActionsCallbackInterface = instance;
            if (instance != null)
            {
                @UP.started += instance.OnUP;
                @UP.performed += instance.OnUP;
                @UP.canceled += instance.OnUP;
                @DOWN.started += instance.OnDOWN;
                @DOWN.performed += instance.OnDOWN;
                @DOWN.canceled += instance.OnDOWN;
                @RIGHT.started += instance.OnRIGHT;
                @RIGHT.performed += instance.OnRIGHT;
                @RIGHT.canceled += instance.OnRIGHT;
                @LEFT.started += instance.OnLEFT;
                @LEFT.performed += instance.OnLEFT;
                @LEFT.canceled += instance.OnLEFT;
                @Exit.started += instance.OnExit;
                @Exit.performed += instance.OnExit;
                @Exit.canceled += instance.OnExit;
            }
        }
    }
    public Player1Actions @Player1 => new Player1Actions(this);

    // Player 2
    private readonly InputActionMap m_Player2;
    private IPlayer2Actions m_Player2ActionsCallbackInterface;
    private readonly InputAction m_Player2_UP;
    private readonly InputAction m_Player2_DOWN;
    private readonly InputAction m_Player2_RIGHT;
    private readonly InputAction m_Player2_LEFT;
    private readonly InputAction m_Player2_Exit;
    public struct Player2Actions
    {
        private @NewControls m_Wrapper;
        public Player2Actions(@NewControls wrapper) { m_Wrapper = wrapper; }
        public InputAction @UP => m_Wrapper.m_Player2_UP;
        public InputAction @DOWN => m_Wrapper.m_Player2_DOWN;
        public InputAction @RIGHT => m_Wrapper.m_Player2_RIGHT;
        public InputAction @LEFT => m_Wrapper.m_Player2_LEFT;
        public InputAction @Exit => m_Wrapper.m_Player2_Exit;
        public InputActionMap Get() { return m_Wrapper.m_Player2; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(Player2Actions set) { return set.Get(); }
        public void SetCallbacks(IPlayer2Actions instance)
        {
            if (m_Wrapper.m_Player2ActionsCallbackInterface != null)
            {
                @UP.started -= m_Wrapper.m_Player2ActionsCallbackInterface.OnUP;
                @UP.performed -= m_Wrapper.m_Player2ActionsCallbackInterface.OnUP;
                @UP.canceled -= m_Wrapper.m_Player2ActionsCallbackInterface.OnUP;
                @DOWN.started -= m_Wrapper.m_Player2ActionsCallbackInterface.OnDOWN;
                @DOWN.performed -= m_Wrapper.m_Player2ActionsCallbackInterface.OnDOWN;
                @DOWN.canceled -= m_Wrapper.m_Player2ActionsCallbackInterface.OnDOWN;
                @RIGHT.started -= m_Wrapper.m_Player2ActionsCallbackInterface.OnRIGHT;
                @RIGHT.performed -= m_Wrapper.m_Player2ActionsCallbackInterface.OnRIGHT;
                @RIGHT.canceled -= m_Wrapper.m_Player2ActionsCallbackInterface.OnRIGHT;
                @LEFT.started -= m_Wrapper.m_Player2ActionsCallbackInterface.OnLEFT;
                @LEFT.performed -= m_Wrapper.m_Player2ActionsCallbackInterface.OnLEFT;
                @LEFT.canceled -= m_Wrapper.m_Player2ActionsCallbackInterface.OnLEFT;
                @Exit.started -= m_Wrapper.m_Player2ActionsCallbackInterface.OnExit;
                @Exit.performed -= m_Wrapper.m_Player2ActionsCallbackInterface.OnExit;
                @Exit.canceled -= m_Wrapper.m_Player2ActionsCallbackInterface.OnExit;
            }
            m_Wrapper.m_Player2ActionsCallbackInterface = instance;
            if (instance != null)
            {
                @UP.started += instance.OnUP;
                @UP.performed += instance.OnUP;
                @UP.canceled += instance.OnUP;
                @DOWN.started += instance.OnDOWN;
                @DOWN.performed += instance.OnDOWN;
                @DOWN.canceled += instance.OnDOWN;
                @RIGHT.started += instance.OnRIGHT;
                @RIGHT.performed += instance.OnRIGHT;
                @RIGHT.canceled += instance.OnRIGHT;
                @LEFT.started += instance.OnLEFT;
                @LEFT.performed += instance.OnLEFT;
                @LEFT.canceled += instance.OnLEFT;
                @Exit.started += instance.OnExit;
                @Exit.performed += instance.OnExit;
                @Exit.canceled += instance.OnExit;
            }
        }
    }
    public Player2Actions @Player2 => new Player2Actions(this);
    public interface IPlayer1Actions
    {
        void OnUP(InputAction.CallbackContext context);
        void OnDOWN(InputAction.CallbackContext context);
        void OnRIGHT(InputAction.CallbackContext context);
        void OnLEFT(InputAction.CallbackContext context);
        void OnExit(InputAction.CallbackContext context);
    }
    public interface IPlayer2Actions
    {
        void OnUP(InputAction.CallbackContext context);
        void OnDOWN(InputAction.CallbackContext context);
        void OnRIGHT(InputAction.CallbackContext context);
        void OnLEFT(InputAction.CallbackContext context);
        void OnExit(InputAction.CallbackContext context);
    }
}
