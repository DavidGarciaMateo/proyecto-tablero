using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class DestroyerController : MonoBehaviour
{

    public GameObject roundCounter1_P1, roundCounter2_P1, roundCounter1_P2, roundCounter2_P2, tiebreak, tiebreakTxt, tiebreak2, TBP1txt, TBP2txt, song1;

    public Text score_P1txt, score_P2txt;

    public Sprite[] tiebreakImages;

    private Sprite newSprite;

    public Animator coconutAnim1, coconutAnim2;
    public GameObject blueTxt;
    public GameObject redTxt;
    public GameObject pressToContinue, mainCamera, blueWins, blueLoses, redWins, redLoses, flotador1, flotador2, player1, player2;
    public GameObject D6P1, D6P2, D4P1, D4P2, DealerP1, DealerP2;
    public P1Controller controller;
    //private Animation coconutAnim;
    //private bool coconut2;

    //IMPORTANTE AQUII
    //Lo pòngo en menos dos porque me detecta los dos circulos que suben con la animacion
    public int destroyNotes = -2;
    private int song1Notes, song2Notes, song3Notes, scoreP1, scoreP2; 
    private string sP1Score, sP2Score;
    private bool alreadyIn= false, TBloaded = false;

   public float timer1 = 0f;

    // Start is called before the first frame update
    void Start()
    {
        song1Notes = song1.transform.childCount; 
        
        blueTxt.SetActive(false);
        redTxt.SetActive(false);
        pressToContinue.SetActive(false);
        
        //coconutAnim1 = gameObject.GetComponent<Animation>();
        //coconutAnim2 = gameObject.GetComponent<Animation>();
        //coconut2 = false;
    }

    // Update is called once per frame
    void Update()
    {

        if(song1Notes == destroyNotes && alreadyIn == false){
            alreadyIn = true;
            
            //sP1Score = score_P1txt.text;
            scoreP1 = int.Parse(score_P1txt.text);
            scoreP2 = int.Parse(score_P2txt.text);
            if(scoreP1 > scoreP2){
                //HACER QUE SE ESPERE UNOS SEGUNDOS PARA QUE DE TIEMPO A VER COMO CAE EL COCO
                coconutAnim1.SetBool("P1Win", true);
                //HACER QUE SE ESPERE UNOS SEGUNDOS PARA QUE DE TIEMPO A VER COMO CAE EL COCO
                blueTxt.SetActive(true);
                //roundCounter1_P1.SetActive(true);
                Debug.Log("Gana el P1");

                //Desactivamos los players originales
                player1.SetActive(false);
                player2.SetActive(false);

                pressToContinue.SetActive(true);
                blueWins.SetActive(true);
                redLoses.SetActive(true);
                flotador1.SetActive(false);
                flotador2.SetActive(false);
                //coconut2 = true;
                score_P1txt.enabled = false;
                score_P2txt.enabled = false;  
                
                D6P1.SetActive(true);
                D4P2.SetActive(true);
                DealerP1.SetActive(true);

                PlayerPrefs.SetString("Preferencia","P1");
                PlayerPrefs.SetString("1D6","P1");
                controller.toExit = true;
                StartCoroutine(WaitToExit());

            }else if(scoreP1 < scoreP2){
                //HACER QUE SE ESPERE UNOS SEGUNDOS PARA QUE DE TIEMPO A VER COMO CAE EL COCO
                coconutAnim2.SetBool("P2Win", true);
                //HACER QUE SE ESPERE UNOS SEGUNDOS PARA QUE DE TIEMPO A VER COMO CAE EL COCO
                redTxt.SetActive(true);
                //roundCounter1_P2.SetActive(true);
                Debug.Log("Gana el P2");
                //Desactivamos los players originales
                player1.SetActive(false);
                player2.SetActive(false);

                pressToContinue.SetActive(true);
                blueLoses.SetActive(true);
                redWins.SetActive(true);
                flotador1.SetActive(false);
                flotador2.SetActive(false);
                //coconut2 = true;
                score_P1txt.enabled = false;
                score_P2txt.enabled = false;
                
                D4P1.SetActive(true);
                D6P2.SetActive(true);
                DealerP2.SetActive(true);

                PlayerPrefs.SetString("Preferencia","P2");
                PlayerPrefs.SetString("1D6","P2");
                controller.toExit = true;
                StartCoroutine(WaitToExit());

            }else if(scoreP1 == scoreP2){
                
                
                tiebreakTxt.SetActive(true);
                tiebreak.SetActive(true);
                tiebreak2.SetActive(true);
                
                TBP1txt.SetActive(true);
                TBP2txt.SetActive(true);
                alreadyIn = false;

                //Lo controlo para que lo haga una vez sino es una locura y no deja de cargar flechas (Puede venir bien en un futuro controlarlo??)
                if(TBloaded==false){
                    //Esto es para generar una flecha random
                    int random = Random.Range(0, tiebreakImages.Length);
                    newSprite = tiebreakImages[random];
                    tiebreak.GetComponent<Image>().sprite = newSprite;
                    TBloaded=true;
                }
                
                
                if(TBP1txt.GetComponent<Text>().text.Equals("10")){
                    coconutAnim1.SetBool("P1Win", true);
                    //roundCounter1_P1.SetActive(true);
                    blueTxt.SetActive(true);

                    tiebreakTxt.SetActive(false);
                    tiebreak.SetActive(false);
                    tiebreak2.SetActive(false);
                
                    TBP1txt.SetActive(false);
                    TBP2txt.SetActive(false);

                    //Desactivamos los players originales
                    player1.SetActive(false);
                    player2.SetActive(false);
                    
                    pressToContinue.SetActive(true);
                    blueWins.SetActive(true);
                    redLoses.SetActive(true);
                    flotador1.SetActive(false);
                    flotador2.SetActive(false);
                    score_P1txt.enabled = false;
                    score_P2txt.enabled = false;

                    D6P1.SetActive(true);
                    D4P2.SetActive(true);
                    DealerP1.SetActive(true);

                    PlayerPrefs.SetString("Preferencia","P1");
                    PlayerPrefs.SetString("1D6","P1");
                    controller.toExit = true;
                    StartCoroutine(WaitToExit());


                }else if(TBP2txt.GetComponent<Text>().text == "10"){
                    coconutAnim2.SetBool("P2Win", true);
                    //roundCounter1_P2.SetActive(true);
                    redTxt.SetActive(true);

                    tiebreakTxt.SetActive(false);
                    tiebreak.SetActive(false);
                    tiebreak2.SetActive(false);
                
                    TBP1txt.SetActive(false);
                    TBP2txt.SetActive(false);

                    //Desactivamos los players originales
                    player1.SetActive(false);
                    player2.SetActive(false);

                    pressToContinue.SetActive(true);
                    blueLoses.SetActive(true);
                    redWins.SetActive(true);
                    flotador1.SetActive(false);
                    flotador2.SetActive(false);
                    score_P1txt.enabled = false;
                    score_P2txt.enabled = false;
                
                    D4P1.SetActive(true);
                    D6P2.SetActive(true);
                    DealerP2.SetActive(true);

                    PlayerPrefs.SetString("Preferencia","P2");
                    PlayerPrefs.SetString("1D6","P2");
                    controller.toExit = true;
                    StartCoroutine(WaitToExit());
                }
            }

        }

        /*if(coconut2 = false)
        {
            coconutAnim.SetBool("CoconutFall2", false);
        }

        if(coconut2 = true)
        {
            coconutAnim.SetBool("CoconutFall2", true);
        }*/
        
    }
    private void OnTriggerEnter2D(Collider2D other){

        destroyNotes++;
    }
private IEnumerator WaitToExit()
{
    yield return new WaitForSeconds (5);
    SceneManager.LoadScene("Tablero");
}

}
