using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    [SerializeField] private GameObject c1;
    [SerializeField] private GameObject c2;
    [SerializeField] private GameObject mC1;
    [SerializeField] private GameObject text3;
    [SerializeField] private GameObject text2;
    [SerializeField] private GameObject text1;
    [SerializeField] private GameObject textStart;
    private GeneralMovement script;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("Controls");
    }
    IEnumerator Controls()
    {

        yield return new WaitForSeconds(3f);
        c1.SetActive(false);
        c2.SetActive(true);
        
        yield return new WaitForSeconds(1.5f);
        c2.SetActive(false);
        mC1.SetActive(true);

        StartCoroutine("TextCountDown");
        StopCoroutine("Controls");
        
    }
    IEnumerator TextCountDown()
    {
        yield return new WaitForSeconds(1f);
        text3.SetActive(true);
        yield return new WaitForSeconds(1f);
        text3.SetActive(false);
        text2.SetActive(true);
        c2.SetActive(false);
        yield return new WaitForSeconds(1f);
        text2.SetActive(false);
        text1.SetActive(true);
        yield return new WaitForSeconds(1f);
        text1.SetActive(false);
        textStart.SetActive(true);
        yield return new WaitForSeconds(1f);
        textStart.SetActive(false);
        StopCoroutine("TextCountDown");

    }
    
}
