using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Character : MonoBehaviour
{
    [Header("Movement Settings")]
    [SerializeField]
    public float speedNormal = 5;
    [SerializeField]
    private float speedOnAirDecrease = 0;
    [SerializeField]
    private float speedTimeFreez = 40f;
    [SerializeField]
    public bool isStoped = false;
    [Header("Jump Sttings")]
    [SerializeField]
    private float gravity = -30f;
    [SerializeField]
    private float forceJump = 1.3f;
    [SerializeField]
    private float hightJump = 3;

    private Transform ubicacion;


    public Character2 char2;
    public bool winP1;
    public bool looseP1;

    [SerializeField]
    public Transform respawn;

    [SerializeField]
    private GameObject acabo;

    private GameObject mesh;

    public bool done= false;

    private bool movement;

    public Final final;

    public Advance advance;

    private Vector2 directionMove;
    private Vector3 velocity;
    private bool pressJump = false;
    private bool speedTimeStop = false;
    private Camera mainCamera;
    [HideInInspector]
    public CharacterController controller;
    [SerializeField] private Transform player;
    private bool bended = false;
    private SphereCollider sphere;

    private PlayerInput inputAction;

    public Animator animator;

    private void Awake()
    {
        controller = GetComponent<CharacterController>();
        inputAction = GetComponent<PlayerInput>();
        animator = GetComponent<Animator>();
    }
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("ActivateMov");
        mesh = GameObject.Find("meshP1");
        sphere = GetComponent<SphereCollider>();
    }

    // Update is called once per frame
    void Update()
    {
        if (directionMove == Vector2.zero)
        {
            animator.SetFloat("movement", 0);
        }
        else
        {
            animator.SetFloat("movement", 1);

        }
        if (speedTimeStop == false)
        {
            PlayerMovement(new Vector3(directionMove.x, 0, directionMove.y), speedNormal, pressJump, forceJump, hightJump);
        }
        if (speedTimeStop == true)
        {
            PlayerMovement(new Vector3(directionMove.x, 0, directionMove.y), speedTimeFreez, pressJump, forceJump, hightJump);
        }
    }
    private void OnMove(InputValue value)
    {

        directionMove = value.Get<Vector2>();
        if (directionMove.x > 0)
        {
            transform.eulerAngles = new Vector3(0, 0, 0);
            //animator.SetFloat("movement", (float)directionMove.x);
        }
        else if (directionMove.x < 0)
        {
            transform.eulerAngles = new Vector3(0, 180, 0);
            //animator.SetFloat("movement", (float)directionMove.x);
        }
        else if (directionMove.y < 0)
        {
            transform.eulerAngles = new Vector3(0, 90, 0);
            //animator.SetFloat("movement", (float)directionMove.x);
        }
        else if (directionMove.y > 0)
        {
            transform.eulerAngles = new Vector3(0, -90, 0);
            //animator.SetFloat("movement", (float)directionMove.x);
        }
        
    }
    private void OnJump(InputValue value)
    {
        pressJump = value.isPressed;
        StartCoroutine("Jumping");
        

    }

    IEnumerator Jumping()
    {
        animator.SetBool("jump", true);
        yield return new WaitForSeconds(0.8f);
        animator.SetBool("jump", false);
    }

    public void PlayerMovement(Vector3 direction, float speed, bool condition, float force, float hight)
    {
        
        if (controller.isGrounded)
        {
            velocity = direction;
            velocity *= speed;
            if (condition)
            {
                animator.SetBool("jump", false);
                
                velocity.y += Mathf.Sqrt(force * hight * Mathf.Abs(gravity));
                pressJump = false;
                
            }
        }
        velocity.y += gravity * Time.deltaTime;
        controller.Move(velocity * Time.deltaTime);
        if (controller.isGrounded == false)
        {
            if (controller.collisionFlags == CollisionFlags.Above)
            {
                velocity.y += gravity * Time.deltaTime;
            }
            Vector3 velocityAir = new Vector3(directionMove.x, 0, directionMove.y);
            velocityAir *= speed - speedOnAirDecrease;
            controller.Move(velocityAir * Time.deltaTime);

        }
    }
    private void OnBend(InputValue value) {

        Debug.Log("Se ha pulsado");
        Debug.Log(bended);
        if (!bended)
        {
            controller.height = 1.2f;
            animator.SetBool("crounch", true);
            bended = true;
        }
        else
        {
            controller.height = 2f;
            animator.SetBool("crounch", false);
            bended = false;

        }
    }
    /*private void OnGrab(InputValue value)
    {
        Debug.Log("Se ha pulsado E");
        RaycastHit hit;

        if (Physics.Raycast(target.position, target.TransformDirection(Vector3.forward), out hit, rayDistance))
        {
            Debug.Log("Hit");
            Debug.Log(hit.collider.name);
            Debug.DrawRay(target.position, target.TransformDirection(Vector3.forward) * hit.distance, Color.red);
            if (hit.collider.gameObject.name== "Character1")
            {
                if (!grabbed)
                {
                    speedNormal = 2;
                    Debug.Log("Coge al Player");
                    grabbed = true;
                }
                else
                {
                    speedNormal = 5;
                    Debug.Log("No Coge al Player");
                    grabbed = false;
                }
            }
        }
    }
    IEnumerator Grabbed()
    {
        speedNormal = 2;
        yield return new WaitForSeconds(2f);
        speedNormal = 5;
        Debug.Log("Grabeado");
    }*/
    IEnumerator ActivateMov()
    {
        animator.SetBool("startanim2", true);
        yield return new WaitForSeconds(3f);
        controller.height = 1.5f;
        animator.SetBool("startanim2", false);
        yield return new WaitForSeconds(6f);
        inputAction.enabled = true;
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Obstacle"))
        {
            StartCoroutine("Respawn");
        }
        if (other.gameObject.CompareTag("Respawn"))
        {
            respawn = other.gameObject.GetComponent<Transform>();
            
        }
        if (other.gameObject.CompareTag("Finish"))
        {
            Debug.Log("Final");
            StartCoroutine("Respawn");
        }
        if (other.gameObject.CompareTag("Floor"))
        {
            print("Se acabo");
            StartCoroutine("SeAcabo");
        }
        if (other.gameObject.CompareTag("BackWall"))
        {
            
            ubicacion=advance.Injection();
            //ubicacion = advance.respawns[value];
            print(ubicacion);
            controller.enabled = false;
            transform.position = new Vector3(ubicacion.position.x, ubicacion.position.y, ubicacion.position.z);
            StartCoroutine("Respawn3");
        }

    }
    private void MovePlayer()
    {
        Vector3 increment = new Vector3(12, 0, 0);
        transform.position = new Vector3(transform.position.x + increment.x, transform.position.y, transform.position.z);
    }
    
    IEnumerator Respawn()
    {
        controller.enabled = false;
        Vector3 pos = new Vector3(respawn.position.x, respawn.position.y, respawn.position.z);
        player.position = pos;
        mesh.SetActive(false);
        yield return new WaitForSeconds(0.1f);
        mesh.SetActive(false);
        yield return new WaitForSeconds(0.1f);
        mesh.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        mesh.SetActive(false);
        yield return new WaitForSeconds(0.1f);
        mesh.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        mesh.SetActive(false);
        yield return new WaitForSeconds(0.1f);
        mesh.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        mesh.SetActive(false);
        yield return new WaitForSeconds(0.1f);
        mesh.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        controller.enabled = true;
    }
    IEnumerator Respawn3()
    {
        
        mesh.SetActive(false);
        yield return new WaitForSeconds(0.1f);
        mesh.SetActive(false);
        yield return new WaitForSeconds(0.1f);
        mesh.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        mesh.SetActive(false);
        yield return new WaitForSeconds(0.1f);
        mesh.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        mesh.SetActive(false);
        yield return new WaitForSeconds(0.1f);
        mesh.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        mesh.SetActive(false);
        yield return new WaitForSeconds(0.1f);
        mesh.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        controller.enabled = true;
    }
    IEnumerator SeAcabo()
    {
        char2.GetComponent<PlayerInput>().enabled = false;
        GetComponent<PlayerInput>().enabled = false;
        GetComponent<CharacterController>().enabled = false;
        char2.GetComponent<CharacterController>().enabled = false;
        PlayerPrefs.SetString("Preferencia", "P1");
        PlayerPrefs.SetString("1D6", "P1");
        winP1 = true;
        char2.winP2 = false;
        acabo.SetActive(true);
        Time.timeScale = 0.2f;
        yield return new WaitForSeconds(0.8f);
        final.Reward();

    }

}
