    using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;
using UnityEngine.UI;
public class Final : MonoBehaviour
{
    public Character char1;
    public Character2 char2;
    
    private AudioSource audioSource;

    [SerializeField] private Text playerWinerText;

    [SerializeField] private Transform p1;
    [SerializeField] private Transform p2;
    [SerializeField] private GameObject finals;
    [SerializeField] private GameObject panel;

    [SerializeField] private GameObject mainCamera;
    [SerializeField] private GameObject deadlys;

    [SerializeField] private Transform winPos;
    [SerializeField] private Transform losePos;

    [SerializeField] private Text seacabo;
    [SerializeField] private AudioClip clipy;


    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        
    }

    public void Reward()
    {
        audioSource.clip = clipy;
        audioSource.Play();
        char1.GetComponent<PlayerInput>().enabled = false;
        char2.GetComponent<PlayerInput>().enabled = false;
        char1.controller.center = new Vector3(0, 1.32f, 0);
        char2.controller.center = new Vector3(0, 1.15f, 0);
        Time.timeScale = 1f;
        finals.SetActive(true);
        deadlys.SetActive(false);
        panel.SetActive(true);
        mainCamera.SetActive(false);
        seacabo.enabled = false;

        if (char1.winP1)
        {
            p1.transform.Rotate(0, 90, 0);
            p2.transform.Rotate(0, 90, 0);
            p1.position = winPos.position;
            p2.position = losePos.position;
            char1.animator.SetBool("winanim", true);
            char2.animator.SetBool("loseanim", true);
            playerWinerText.text = "Player1 " + "Win";
        }
        else
        {
            char1.controller.height = 1.86f;
            p1.transform.Rotate(0, 90, 0);
            p2.transform.Rotate(0, 90, 0);
            char2.animator.SetBool("winanim", true);
            char1.animator.SetBool("loseanim", true);
            p1.position = losePos.position;
            p2.position = winPos.position;
            playerWinerText.text = "Player2 " + "Win";
        }
    }
    public void ExitMinigame()
    {
        SceneManager.LoadScene("Tablero");
    }
}
