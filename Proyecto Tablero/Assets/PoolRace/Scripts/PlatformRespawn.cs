using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformRespawn : MonoBehaviour
{
    private float[] platProbs = { 25, 25, 25,25 };
    [SerializeField] private GameObject platform;
    [SerializeField] private Transform objective;
    [SerializeField] private Transform objective1;
    [SerializeField] private Transform objective2;
    [SerializeField] private Transform objective3;
    [SerializeField] private Transform final;
    GameObject platformInstantiate = null;
    public int respawn;
    private bool spawnNumber = false;
    private bool spawnNumber1 = false;
    private bool spawnNumber2 = false;
    private bool spawnNumber3 = false;
    private Transform ob;
    private Vector2 respawnInc;
    private GeneralMovement script;
    [SerializeField] private GameObject platFinal;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(StartPawn());
    }

        // Update is called once per frame
    void Update()
    {

    }

    IEnumerator StartPawn()
    {

                int numPlats;
                numPlats = Random.Range(2, 4);
                for (int i = 0; i < numPlats; i++)
                {
                    float probPlat = ProbabilitySystem(platProbs);

                    switch (probPlat)
                    {
                        
                        case 0:
                            if (!spawnNumber)
                            {

                                spawnNumber = true;
                            }else if (!spawnNumber1)
                            {

                                spawnNumber1 = true;
                            }else if (!spawnNumber2)
                            {

                                spawnNumber2 = true;
                            }else if (!spawnNumber3)
                            {

                                spawnNumber3 = true;
                            }
                    //platformInstantiate = platform;
                    /*
                    actualPlats++;*/
                    break;
                        case 1:
                            if (!spawnNumber1)
                            {

                                spawnNumber1 = true;
                            }else if (!spawnNumber)
                            {
                             
                                spawnNumber = true;
                            }else if (!spawnNumber2)
                            {
                          
                                spawnNumber2 = true;
                            }else if (!spawnNumber3)
                            {
                          
                                spawnNumber3 = true;
                            }

                    //platformInstantiate = extra;
                    /*
                    actualPlats++;   */
                    break;
                        case 2:
                            if (!spawnNumber2)
                            {
                            
                                spawnNumber2 = true;
                            }else if (!spawnNumber)
                            {
                            
                                spawnNumber = true;
                            }else if (!spawnNumber1)
                            {
                             
                                spawnNumber1 = true;
                            }else if (!spawnNumber3)
                            {
                             
                                spawnNumber3 = true;
                            }

                    //print(spawnNumber2);

                            break;
                        case 3:
                            if (!spawnNumber3)
                            {
                              
                                spawnNumber3 = true;
                            }else if (!spawnNumber2)
                            {
                           
                                spawnNumber2 = true;
                            }else if (!spawnNumber1)
                            {
                              
                                spawnNumber1 = true;
                            } else if (!spawnNumber)
                            {
                             
                                spawnNumber = true;
                            }


                    //platformInstantiate = extra;
                    /*
                        actualPlats++;*/
                    break;
                    }
                    

                }
                if (spawnNumber)
                    {

                        Instantiate(platform, objective.position, transform.rotation);

                        //spawnNumber = false;
                    }
                    if (spawnNumber1)
                    {

                        Instantiate(platform, objective1.position, transform.rotation);


                        //spawnNumber1 = false;
                    }
                    if (spawnNumber2)
                    {

                        Instantiate(platform, objective2.position, transform.rotation);

                        //spawnNumber2 = false;
                    }
                    if (spawnNumber3)
                    {

                        Instantiate(platform, objective3.position, transform.rotation);

                        //spawnNumber3 = false;
                    }
                spawnNumber = false;
                spawnNumber1 = false;
                spawnNumber2 = false;
                spawnNumber3 = false;
                respawn++;
                float distance = -6f;
                Vector3 respawnPosition = transform.position;
                respawnPosition.x -= distance;
                transform.position = respawnPosition;
                //print(respawn);
                yield return new WaitForSeconds(2f);
            if (respawn < 15)
            {
                StartCoroutine(StartPawn());
            }
            else
            {
                platFinal.SetActive(true);
            }
    }

    float ProbabilitySystem(float[] probs)
    {

        float total = 0;

        foreach (float elem in probs)
        {
            total += elem;
        }

        float randomPoint = Random.value * total;

        for (int i = 0; i < probs.Length; i++)
        {
            if (randomPoint < probs[i])
            {
                return i;
            }
            else
            {
                randomPoint -= probs[i];
            }
        }
        return probs.Length - 1;
    }
}
