using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public Text Score_txt, Score_txt2;
    public int CurrentScore, CurrentScore2;
    public int eachZone;
    // Start is called before the first frame update

    public void AddingScore(int amount)
    {
        eachZone = amount;
        if (eachZone == 0)
        {
            CurrentScore += 1;
            Score_txt.text = "Player 1:" + CurrentScore;
        }
        if (eachZone == 1)
        {
            CurrentScore2 += 1;
            Score_txt2.text = "Player 2:" + CurrentScore2;
        }
    }
}
