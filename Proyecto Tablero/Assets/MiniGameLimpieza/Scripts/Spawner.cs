using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    float count = 3;
    public GameObject obstacle, Point1, Point2, Point3, Point4;
    public float Timer;

    public bool gaming;
    // Start is called before the first frame update
    private void Start()
    {
        gaming = false;
    }
    void Update()
    {
        if (gaming)
        {
            count += Time.deltaTime;
            if (count >= 4)
            {
                Vector3 RandomSpawn = new Vector3(0,0,0);
                float z = Random.Range(0,4);
                if (z == 0)
                    RandomSpawn = Point1.transform.position;
                if (z == 1)
                    RandomSpawn = Point2.transform.position;
                if (z == 2)
                    RandomSpawn = Point3.transform.position;
                if (z == 3)
                    RandomSpawn = Point4.transform.position;

                GameObject newgo =  Instantiate(obstacle, RandomSpawn, transform.rotation);
                Destroy(newgo, 20f);

                count = 0;
            }
        }
    }
}
