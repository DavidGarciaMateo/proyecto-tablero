using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManagerCasco : MonoBehaviour
{
    [SerializeField]private float coutn = 60;
    public Text timer;

    public ScoreManager socoreMan;
    public EGamePanel endPanel;

    public bool player1Wins, player2Wins;

    public GameObject player1Score, player2Score, timeText, UltimaFregadaTxt;

    public GameObject cam1, cam2, CountDown;

    public CharController player1;
    public CharController2 player2;
    public Spawner[] spawn;
    private bool started;
    public bool endTie;

    public AudioSource countDown;
    // Update is called once per frame
    private void Start()
    {
        StartCoroutine(StartAnimation());
        started = false;
        endTie = false;
    }
    void Update()
    {
        if (started)
        {
            coutn -= Time.deltaTime;
            timer.text = "Time: " + coutn.ToString("f0");

            if (coutn <= 0f)
            {
                player1Score.SetActive(false);
                player2Score.SetActive(false);
                timeText.SetActive(false);
                UltimaFregadaTxt.SetActive(true);

                if (socoreMan.CurrentScore > socoreMan.CurrentScore2)
                {
                    player1Wins = true;
                    player2Wins = false;
                    endPanel.MatchFinish2(player1Wins, player2Wins);
                    UltimaFregadaTxt.SetActive(false);
                }
                else if(socoreMan.CurrentScore2 > socoreMan.CurrentScore)
                {
                    player1Wins = false;
                    player2Wins = true;
                    endPanel.MatchFinish2(player1Wins, player2Wins);
                    UltimaFregadaTxt.SetActive(false);
                }
                if (socoreMan.CurrentScore2 == socoreMan.CurrentScore)
                {
                    endTie = true;
                }
            }
        }
    }
    IEnumerator StartAnimation()
    {
        cam1.SetActive(true);

        yield return new WaitForSeconds(2f);

        cam1.SetActive(false);
        cam2.SetActive(true);

        yield return new WaitForSeconds(2f);

        cam2.SetActive(false);
        CountDown.SetActive(true);
        countDown.Play();
        yield return new WaitForSeconds(4f);
        CountDown.SetActive(false);

        started = true;
        player1.gamingChar1 = true;
        player2.gamingChar2 = true;

        for (int i = 0; i < spawn.Length; i++)
        {
            spawn[i].gaming = true;
        }
    }
}
