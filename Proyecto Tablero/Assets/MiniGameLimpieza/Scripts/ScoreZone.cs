using UnityEngine;

public class ScoreZone : MonoBehaviour
{
    public ScoreManager scoreMan;
    public ScoreDropper scoreDrop;

    public int eachZone;

    public AudioSource scoreUp;
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "score1")
        {
            scoreUp.Play();

            scoreMan.AddingScore(eachZone);
            scoreDrop.Drop();
            Destroy(other.gameObject);
        }
    }
}
