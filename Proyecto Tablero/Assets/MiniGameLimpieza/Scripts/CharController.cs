using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;

public class CharController : MonoBehaviour
{
    public CharacterController controller;
    public ScoreManager scoreMan;
    private Collider Player;
    public float speed = 6f;

    public float turnSmoothTime = 0.1f;
    float turnSmoothVelocity;

    private bool Hitted;
    public bool gamingChar1;

    private Vector3 direction;

    public PlayerInput playerActions;
    private bool grabbing;
    // Update is called once per frame
    private void Start()
    {
        gamingChar1 = false;
    }
    void Update()
    {
        if (gamingChar1)
        {           
            Vector3 Gravity = new Vector3(0, -1, 0).normalized;

            controller.Move(Gravity * 9 * Time.deltaTime);
            if (direction.magnitude >= 0.1f)
            {
                float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg;
                float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);
                transform.rotation = Quaternion.Euler(0f, angle, 0f);

                controller.Move(direction * speed * Time.deltaTime);
            }
            if (Hitted)
            {
                Onhit();
                Hitted = false;
            }
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if(other.tag == "score1")
        {
            if (grabbing)
            {
                other.transform.parent = this.transform;
                other.GetComponent<BoxCollider>().enabled = false;
                other.GetComponent<Rigidbody>().isKinematic = true;
            }
            else
            {
                other.transform.parent = null;
                other.GetComponent<BoxCollider>().enabled = true;
                other.GetComponent<Rigidbody>().isKinematic = false;
            }
        }
        else if(other.tag == "Pleyer")
        {
            if (grabbing)
            {
                Player = other;
                Hitted = true;
            }
        }
    }
    IEnumerator OnPlayerHit()
    {
        yield return new WaitForSeconds(1);

        Player.GetComponent<CharController2>().enabled = true;
        Player.GetComponent<CharacterController>().enabled = true;
        Destroy(Player.GetComponent<CharController2>().gameObject.GetComponent<Rigidbody>());
    }
    private void Onhit()
    {
        if (Player.tag == "Pleyer")
        {
            Player.GetComponent<CharController2>().enabled = false;
            Player.GetComponent<CharacterController>().enabled = false;
            Player.GetComponent<CharController2>().gameObject.AddComponent<Rigidbody>();
            Vector3 forceDir = Player.transform.position;
            forceDir.y += 2f;
            Player.GetComponent<CharController2>().gameObject.GetComponent<Rigidbody>().AddForce((Player.transform.position - transform.position) * 10, ForceMode.Impulse);

            StartCoroutine(OnPlayerHit());
        }
    }
    public void OnMovement(InputAction.CallbackContext value)
    {
        Vector2 inputMovement = value.ReadValue<Vector2>();
        direction = new Vector3(inputMovement.x, 0f, inputMovement.y).normalized;
    }
    public void OnGrab(InputAction.CallbackContext value)
    {
        if (value.started)
        {
            grabbing = true;
        }
        else
        {
            grabbing = false;
        }

    }
}
