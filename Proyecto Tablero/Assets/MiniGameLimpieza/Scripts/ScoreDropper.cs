using UnityEngine;

public class ScoreDropper : MonoBehaviour
{
    public GameObject score1;
    public Transform xinitPoint, xendPoint, zinitPoint, zendPoint;
    private Vector3 SpawnPoint;
    private float x, z, y;

    private int randomObject;

    public void Drop()
    {
        randomObject = Random.Range(0, 5);

        x = Random.Range(xinitPoint.position.x, xendPoint.position.x);
        z = Random.Range(zinitPoint.position.z, zendPoint.position.z);
        y = 9;

        SpawnPoint = new Vector3(x, y, z);
 
        GameObject newgo = Instantiate(score1, SpawnPoint, Quaternion.identity);

        newgo.transform.GetChild(randomObject).gameObject.SetActive(true);
    }
}
