using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public Rigidbody Body;
    Vector3 Speed  = new Vector3(50, 50 ,50);
    public Vector3 rot;
    private Collider Player;
    public bool Timer = false;
    private bool Hitted;
    private GameManagerCasco gameManager;
    private EGamePanel endPanel;

    private void Start()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManagerCasco>();
        endPanel = GameObject.Find("Canvas").transform.GetChild(0).GetComponent<EGamePanel>();
    }
    void Update()
    {
         Body.AddForce(rot, ForceMode.Acceleration);

        if(Body.velocity.magnitude > 50)
        {
            Body.velocity = Body.velocity.normalized * 50;
        }
        if (Hitted)
        {
            Onhit();
            Hitted = false;
        }
    }
    IEnumerator OnPlayerHit()
    {     
        yield return new WaitForSeconds(1);
        if (Player.tag == "Player")
        {
            Player.GetComponent<CharController>().enabled = true;
            Player.GetComponent<CharacterController>().enabled = true;
            Destroy(Player.GetComponent<CharController>().gameObject.GetComponent<Rigidbody>());
        }
        if (Player.tag == "Pleyer")
        {
            Player.GetComponent<CharController2>().enabled = true;
            Player.GetComponent<CharacterController>().enabled = true;
            Destroy(Player.GetComponent<CharController2>().gameObject.GetComponent<Rigidbody>());
        }
    }
    private void Onhit()
    {
        if (Player.tag == "Player")
        {
            Player.GetComponent<CharController>().enabled = false;
            Player.GetComponent<CharacterController>().enabled = false;
            Player.GetComponent<CharController>().gameObject.AddComponent<Rigidbody>();
            Vector3 forceDir = Player.transform.position;
            forceDir.y += 2f;
            Player.GetComponent<CharController>().gameObject.GetComponent<Rigidbody>().AddForce((forceDir - transform.position) * 2, ForceMode.Impulse);

            StartCoroutine(OnPlayerHit());   
        }
        if (Player.tag == "Pleyer")
        {
            Player.GetComponent<CharController2>().enabled = false;
            Player.GetComponent<CharacterController>().enabled = false;
            Player.GetComponent<CharController2>().gameObject.AddComponent<Rigidbody>();
            Vector3 forceDir = Player.transform.position;
            forceDir.y += 2f;
            Player.GetComponent<CharController2>().gameObject.GetComponent<Rigidbody>().AddForce((forceDir - transform.position) * 2, ForceMode.Impulse);

            StartCoroutine(OnPlayerHit());   
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Player = other;
            Hitted = true;

            if (gameManager.endTie)
            {
                gameManager.endTie = false;
                gameManager.player1Wins = false;
                gameManager.player2Wins = true;

                endPanel.MatchFinish2(gameManager.player1Wins, gameManager.player2Wins);
            }
        }
        if (other.tag == "Pleyer")
        {
            Player = other;
            Hitted = true;

            if (gameManager.endTie)
            {
                gameManager.endTie = false;
                gameManager.player1Wins = true;
                gameManager.player2Wins = false;

                endPanel.MatchFinish2(gameManager.player1Wins, gameManager.player2Wins);
            }
        }
    }
}
