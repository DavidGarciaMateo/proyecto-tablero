using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Weapons : ItemID
{
[SerializeField] protected float pushPower = 5f;   
[SerializeField] protected float distanceToHit; 
[ SerializeField]protected CharacterController ccPlayer;
public CharacterController ccTarget;
protected Animator targetAnimator;


    [SerializeField] protected bool equiped = false;
   [SerializeField] protected Vector3 offsetPosition;
    public bool canCheck;
    [SerializeField] protected LayerMask layerMask;
    [SerializeField] protected Vector3 offsetScale;


    private void Awake() {
    ccPlayer = GetComponentInParent<CharacterController>();
    targetAnimator = ccTarget.GetComponent<Animator>();
}

private void OnTriggerStay(Collider other) {
  /*  if(other.CompareTag("Player") && ccTarget.GetComponent<PLController>().PlayerType == other.GetComponent<PLController>().PlayerType)
    {

      // ccTarget.SimpleMove ((ccTarget.transform.localPosition-ccPlayer.transform.localPosition) *pushPower);
        ccTarget.SimpleMove (new Vector3(0,0,(ccTarget.transform.localPosition.z-ccPlayer.transform.localPosition.z)*pushPower));
        targetAnimator.SetTrigger("Hited");
        GetComponent<Collider>().enabled = false;
        
    }    */
   /* if(other.CompareTag("Weapon") && ccPlayer.GetComponent<PLController>().blocking)
    {
        targetAnimator.SetTrigger("Hited");
    }*/
      

    }

void Update()
{
    if (ccPlayer.GetComponent<PLController>().IsAttaquing && ccTarget.GetComponent<PLController>().Defended==false)
    {
    ccTarget.gameObject.GetComponent<Animator>().applyRootMotion =true;
    targetAnimator.SetTrigger("Hited");
    ccTarget.SimpleMove (new Vector3(0,0,(ccTarget.transform.localPosition.z-ccPlayer.transform.localPosition.z)*pushPower));

    canCheck = false;
    ccPlayer.GetComponent<PLController>().IsAttaquing =false;
    StartCoroutine(TargetStuned(GameManagerPFB.Instance.timeAtackStuned));

    }
}
private IEnumerator TargetStuned(float timeStuned)
{
    ccTarget.GetComponent<PlayerInput>().enabled = false;
    yield return new WaitForSeconds(timeStuned);
    ccTarget.GetComponent<PlayerInput>().enabled = true;

}
    
    

}
