using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasCT : MonoBehaviour
{
    [SerializeField] private Text txtWin;
    [SerializeField] private Image staminaBar1;
    [SerializeField] private Image staminaBar2;
   


    
    void Update()
    {
        staminaBar1.fillAmount = GameManagerPFB.Instance.player1.GetComponent<PLController>().Stamina / GameManagerPFB.Instance.player1.GetComponent<PLController>().MaxStamina;
        staminaBar2.fillAmount = GameManagerPFB.Instance.player2.GetComponent<PLController>().Stamina / GameManagerPFB.Instance.player1.GetComponent<PLController>().MaxStamina;
        
    }

        public void MatchFinish()
    {
            GameManagerPFB.Instance.player1.gameObject.GetComponent<PLController>().enabled =false;
            GameManagerPFB.Instance.player2.gameObject.GetComponent<PLController>().enabled =false;
            GameManagerPFB.Instance.player1.enabled=false;
            GameManagerPFB.Instance.player2.enabled=false;
        if(GameManagerPFB.Instance.pointsPL1 >= GameManagerPFB.Instance.pointsToWin)
        {
            txtWin.gameObject.SetActive(true);
            txtWin.text = "Player1 "+ "Win";
            GameManagerPFB.Instance.AnimationEnd(GameManagerPFB.Instance.vCam1, GameManagerPFB.Instance.track1, GameManagerPFB.Instance.LookAt1, GameManagerPFB.Instance.player1.GetComponent<Animator>());
        }
        else if (GameManagerPFB.Instance.pointsPL2 >= GameManagerPFB.Instance.pointsToWin)
        {
            txtWin.gameObject.SetActive(true);
            txtWin.text ="Player2 "+ "Win";
           GameManagerPFB.Instance.AnimationEnd(GameManagerPFB.Instance.vCam2, GameManagerPFB.Instance.track2,GameManagerPFB.Instance.LookAt2,GameManagerPFB.Instance.player2.GetComponent<Animator>());

        }
    }

}
