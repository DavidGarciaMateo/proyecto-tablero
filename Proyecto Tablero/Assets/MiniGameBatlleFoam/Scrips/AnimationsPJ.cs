using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class AnimationsPJ : MonoBehaviour
{
    [SerializeField]private Animator attackAnim;
    [SerializeField] private GameObject rightHand;
    private void OnAttack(InputValue value)
    {

        if(rightHand.transform.childCount==1)
        {
            int childNumber = attackAnim.gameObject.transform.childCount;
            if (childNumber != 0 && attackAnim.GetCurrentAnimatorStateInfo(0).IsName("11"))
            {

                attackAnim.SetTrigger("Attack");

            }
        }
    }

}
