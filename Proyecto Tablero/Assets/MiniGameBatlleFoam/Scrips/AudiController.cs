using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AudiController : MonoBehaviour
{

    [SerializeField] private AudioSource audioSource;
    [SerializeField] private AudioClip[] clips;
    private bool audiGameOn = false;
    // Start is called before the first frame update
    void Start()
    {
        //audioSource.GetComponent<AudioSource>();
        audioSource.clip = clips[0];
        audioSource.Play();
    }

    public void IngameSondOn()
    {
        audioSource.clip = clips[1];
        audioSource.loop = true;
        audioSource.Play();
    }
}
