using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ScritebleObjects", menuName ="Weapons/NormalWeapons", order = 1)]
public class SOWeapons : ScriptableObject
{
[SerializeField]private string type;
[SerializeField]private GameObject weapon;
[SerializeField]private GameObject pickUpWeapon;
}
