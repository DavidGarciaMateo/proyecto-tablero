using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Audio;

public class PLController : MonoBehaviour
{
    public enum Player {Player1, Player2};
    [SerializeField] private Player player;

     [Header("Movement Settings")]
    [SerializeField]
    private float speedNormal = 15;
    [SerializeField]
    private float speedOnAirDecrease = 10;
    [SerializeField]
    public bool isStoped = false;
    [Header("Jump Sttings")]
    [SerializeField]
    private float gravity = -9.8f;
    [SerializeField]
    private float forceJump = 10;
    [SerializeField]
    private float hightJump = 3;

    [SerializeField]
    public int maxLife = 50;
    public int actualLife = 25;
    [SerializeField] GameObject weapon;

    private Vector2 directionMove;
    private Vector3 velocity;
    private bool pressJump = false;
    private CharacterController controller;
    public bool arrastrar;
    private bool arrastrar1;
    private bool arrastrar2;
    private bool wepaonOn = false;
    private bool gooingBack = false;
    private bool lookingFront = true;
    public bool blocking = false;
    private bool isAttaquing;
    private bool defended = false;

   [SerializeField] private bool dodgeing = false;

    [SerializeField] private Transform holdItem;

  
    [SerializeField] private Transform rightHand;
    [SerializeField] private ItemID weapons2;
    [SerializeField] private CanvasCT canvasController;
    [SerializeField] private WinProcess winProcess;
    [SerializeField] private CharacterController target;
    [Header("AnimationsSettings")]
    [SerializeField] private Animator playerAnimator;
    [SerializeField] private Transform rollinEndPos;
    [SerializeField] private float distanceToHit;
    [SerializeField] private int maxComboAttacks = 3;
    [SerializeField] private int comboAtacks = 0;
    
    [Header("StaminaSettings")]
    [SerializeField] private float stamina;
    [SerializeField] private float maxStamina = 10;
    [SerializeField] private float recoverdSpeed = 0.5f;
    [SerializeField] private int attackConsum;
    [SerializeField] private int dodgeConsum;
    [SerializeField] private int blockConsum;

    private Coroutine coldDownCoruntine;

    [SerializeField] private float distanceToblock = 10f;
    [SerializeField] private AudioSource hitAudioSource;
    [SerializeField] private AudioClip hitClip;

    public Player PlayerType { get => player;}
    public bool IsAttaquing { get => isAttaquing; set => isAttaquing = value; }
    public float DistanceToHit { get => distanceToHit; set => distanceToHit = value; }
    public float Stamina { get => stamina; set => stamina = value; }
    public float MaxStamina { get => maxStamina;}
    public GameObject Weapon { get => weapon; set => weapon = value; }
    public bool Defended { get => defended;}

    void Start()
    {
        controller = GetComponent<CharacterController>();
        //playerAnimator = GetComponent<Animator>();
        StartCoroutine(GameManagerPFB.Instance.RestPositionCorrutine());
        Stamina = MaxStamina;


    }

    // Update is called once per frame
    void Update()
    {
        PlayerMovement(new Vector3(directionMove.y, 0, directionMove.x), speedNormal, pressJump, forceJump, hightJump);
        transform.LookAt (new Vector3 (target.transform.position.x, transform.position.y, target.transform.position.z));
        if(Stamina < MaxStamina)
        {
        RestoreStamina();
        }
    }
  private void OnMove(InputValue value)
   {
        directionMove = value.Get<Vector2>();
    }
    private void OnJump(InputValue value)
    {
        pressJump = value.isPressed;
    }

    public void PlayerMovement(Vector3 direction, float speed, bool condition, float force, float hight)
    {
        if (controller.isGrounded)
        {
            LookRotation();
            LookDirection();
            velocity = direction;
            velocity *= speed;
            
            if(lookingFront == false){  
            playerAnimator.SetFloat("Vertical", directionMove.x*-1);
            }
            else{
            playerAnimator.SetFloat("Vertical", directionMove.x);
            }
            if (condition)
            {
                velocity.y += Mathf.Sqrt(force * hight * Mathf.Abs(gravity));
                pressJump = false;
            }
        }
        velocity.y += gravity * Time.deltaTime;
        controller.Move(velocity * Time.deltaTime);
        if (controller.isGrounded == false)
        {
            if (controller.collisionFlags == CollisionFlags.Above)
            {
                velocity.y += gravity * Time.deltaTime;
            }
            Vector3 velocityAir = new Vector3(directionMove.x*-1, 0, directionMove.y*-1);
            velocityAir *= speed - speedOnAirDecrease;
            controller.Move(velocityAir * Time.deltaTime);

        }
        
    }
    private void LookDirection()
    {
        if(directionMove.x <= -0.1f)
        {
            gooingBack = true;
        }
        else if(directionMove.x >= 0.1f)
        {
            gooingBack = false;
        }
    }
    private void LookRotation()
    {
        if(transform.rotation.y> 0.7f){  
        lookingFront = false;
        }
        else{
        lookingFront = true;
        }
    }
    private void OnDodge(InputValue value)
    {
        if(Stamina>= dodgeConsum)
        {
        
        dodgeing=true;

        playerAnimator.applyRootMotion = true;
        controller.GetComponent<PLController>().enabled = false;
        Physics.IgnoreCollision(controller,target);
        
        if(!gooingBack && lookingFront)
        {
            playerAnimator.SetTrigger("Rolling");

        }
        if(gooingBack && !lookingFront)
        {
            playerAnimator.SetTrigger("Rolling");
        }
        if(gooingBack && lookingFront)
        {
            playerAnimator.SetTrigger("BackJump");
        }
        if(!gooingBack && !lookingFront)
        {
            playerAnimator.SetTrigger("BackJump");
        }

        Stamina = Stamina - dodgeConsum;
        }
    }
 
    public void EndAnimation()
    {
        controller.GetComponent<PLController>().enabled = true;
        playerAnimator.applyRootMotion = false;
        Physics.IgnoreCollision(controller,target,false);
        transform.localRotation = Quaternion.identity;
        playerAnimator.SetBool("Block",false);
        blocking = false;
        playerAnimator.SetBool("Attack",false);
        isAttaquing= false;
        defended = false;
        dodgeing = false;


    }   
    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("MatchCollider"))
        {
            GameManagerPFB.Instance.endGame=true;
            EndAnimation();
            StartCoroutine(GameManagerPFB.Instance.RestPositionCorrutine());
            stamina= maxStamina;

            if(player == Player.Player1)
            {
                GameManagerPFB.Instance.pointsPL2++;
                GameManagerPFB.Instance.player2Win = true;
                canvasController.MatchFinish();
                playerAnimator.SetFloat("Vertical",0);
            }
            else if (player == Player.Player2)
            {
                GameManagerPFB.Instance.pointsPL1++;
                GameManagerPFB.Instance.player1Win = true;
                canvasController.MatchFinish();
                playerAnimator.SetFloat("Vertical",0);
            }
        }
    }
    private void OnTriggerStay(Collider other) {
        
        if(player == Player.Player2 && arrastrar1)
        {
            other.transform.SetParent(transform);
        }
         if(player == Player.Player1 && arrastrar2)
        {
            other.transform.SetParent(transform);
        }
        if(other.CompareTag("Weapon")&&wepaonOn)
        {
           if(holdItem.childCount==1)
           {   
               holdItem.GetChild(0).gameObject.SetActive(true);
               holdItem.DetachChildren();
               for(int i=0; i< rightHand.childCount; i++)
               {
                   if(rightHand.GetChild(i).gameObject.activeSelf == true)
                   {
                       rightHand.GetChild(i).gameObject.SetActive(false);
                   }
               }
               

           }
            for (int i = 0; i< rightHand.childCount; i++)
            {
                if(other.GetComponent<ItemID>().weaponType == rightHand.GetChild(i).GetComponent<ItemID>().weaponType)
                {
                    Debug.Log("A");
                    Weapon =  rightHand.GetChild(i).gameObject;
                    rightHand.GetChild(i).gameObject.SetActive(true);
                    other.transform.SetParent(holdItem);
                    other.gameObject.SetActive(false);
                    break;
                }
            } 
        }
    }
   private void OnEquipar(InputValue value)
    {
        wepaonOn = value.isPressed;

    }

    
    private void OnAttack(InputValue value)
    {
        if(Weapon.activeSelf==true)
        {
            if(Stamina >= attackConsum && dodgeing== false)
            {
                hitAudioSource.PlayOneShot(hitClip);
                if(comboAtacks <= maxComboAttacks-1)
                {
                playerAnimator.SetBool("Attack",true);
                 //Stamina = Stamina-attackConsum;
                }
                if(comboAtacks >=0 && coldDownCoruntine == null)
                {
                coldDownCoruntine = StartCoroutine(ColdDownAttack());
                }
            
            }
        }
        
    }
    //colocado en animacion para controlar cualdo sucede
    public void AtacktStaminaSpend()
    {
        Stamina = Stamina-attackConsum;
    }

    private void OnBlock(InputValue value)
    {
        if(Stamina >= blockConsum)
        {
            
          //  controller.GetComponent<PLController>().enabled = false;
            //playerAnimator.SetBool("Block",value.isPressed);
            playerAnimator.SetBool("Block",true);
            blocking = true;
            defended = true;

            if (blocking && target.GetComponent<PLController>().IsAttaquing && Vector3.Distance(transform.position,target.transform.position)< distanceToblock)
            {
               defended = true;
           
                //target.GetComponent<Animator>().SetTrigger("Hited");
               blocking = false;
            }
            
        Stamina = Stamina-blockConsum;
        }
    }

    public void WeponColliderOff()
    {
        //weapon.GetComponent<Collider>().enabled = false;
        Weapon.GetComponent<Weapons>().canCheck = false;
    }
    public void WeponColliderOn()
    {
         if(Vector3.Distance(transform.position, target.transform.position)< DistanceToHit)
        {
            IsAttaquing = true;
            
        }

        Weapon.GetComponent<Weapons>().canCheck = true;
        comboAtacks++;
    }
    private void OnArrastrar(InputValue value)
    {
        arrastrar= value.isPressed;
        
    }   private void OnArrastrar1(InputValue value)
    {
        arrastrar1= value.isPressed;
        
    }
       private void OnArrastrar2(InputValue value)
    {
        arrastrar2= value.isPressed;
        
    }
    private IEnumerator ColdDownAttack()
    {
        yield return new WaitForSeconds(2);
        comboAtacks =0;
        coldDownCoruntine = null;
    }


    private void RestoreStamina()
    {
       Stamina = Stamina + (recoverdSpeed* Time.deltaTime);
    }


}
