using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationsController : MonoBehaviour
{
    public Animation roll;
    [SerializeField] private float rollAnimationSpeed;
    // Start is called before the first frame update
    void Start()
    {
         roll["Rolling"].speed = rollAnimationSpeed;
    }

    // Update is called once per frame
    void Update()
    {
       
    }
}
