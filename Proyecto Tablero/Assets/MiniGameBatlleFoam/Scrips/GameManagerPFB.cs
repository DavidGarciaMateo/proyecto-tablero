using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class GameManagerPFB : MonoBehaviour
{
   public static GameManagerPFB Instance;
    public CharacterController player1;
    public CharacterController player2;
    public int pointsPL1 = 0;
    public int pointsPL2 = 0;
    public int pointsToWin = 3;

    //public Collider machCollider;

    public Transform iniPosPl1;
    public Transform iniPosPl2;
    public GameObject[] weaponsToAppear;

    public float timeAtackStuned =0.5f;

    [SerializeField] private float weponTime = 60;


    [Header("SettingsAnimEnd")]
    [SerializeField] private Animator endAnim;
    [SerializeField] private CinemachineVirtualCamera virtualCamera;
    public GameObject vCam1;
    public GameObject vCam2;
    public CinemachineSmoothPath track1;
    public CinemachineSmoothPath track2;
    public Transform LookAt1;
    public Transform LookAt2;
    public bool endGame = false;
    public bool player1Win = false;
    public bool player2Win = false;
    void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
    }

    public IEnumerator RestPositionCorrutine()
    {
        player1.GetComponent<PLController>().enabled =false;
        player2.GetComponent<PLController>().enabled =false;    
        player1.transform.position = iniPosPl1.position;
        player1.transform.rotation = iniPosPl1.rotation;
        player2.transform.rotation = iniPosPl2.rotation;
        player2.transform.position = iniPosPl2.position;
        yield return new WaitForSeconds(1);
        if(endGame==false)
        {
        player1.GetComponent<PLController>().enabled =true;
        player2.GetComponent<PLController>().enabled =true;
        }

    }
   /* public IEnumerator FallingWeapons()
    {
        yield return new WaitForSeconds(weponTime);  
        weaponsToAppear[0].SetActive(true);
       yield return new WaitForSeconds(10);
        weaponsToAppear[1].SetActive(true);
        yield return new WaitForSeconds(5);
        weaponsToAppear[2].SetActive(true);
    }*/
    public void AnimationEnd(GameObject camGoPLayer, CinemachineSmoothPath trackPlayer,Transform lookAtTr, Animator winerAnimator)
    {
        player1.GetComponent<PLController>().Weapon.gameObject.SetActive(false);
        player2.GetComponent<PLController>().Weapon.gameObject.SetActive(false);

        player1.GetComponent<PLController>().enabled = false;
        player2.GetComponent<PLController>().enabled = false;

        camGoPLayer.SetActive(true);
        Camera.main.enabled=false;
        virtualCamera.m_LookAt = lookAtTr;
        virtualCamera.GetCinemachineComponent<CinemachineTrackedDolly>().m_Path = trackPlayer;
        winerAnimator.SetTrigger("Winner");
        endAnim.SetTrigger("endAnim");

    }
    
}
